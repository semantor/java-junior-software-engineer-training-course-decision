package com.gitlab.semantor.testing;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


class Calculator {
    private final static Logger log = LogManager.getLogger("Calculator");

    /**
     * Calculating the sum of two numbers
     *
     * @param firstArgument  first element
     * @param secondArgument second element
     * @return sum
     * @throws IllegalArgumentException if one of the numbers is {@code Double.NaN} or {@code Double.Infinity}
     */
    static double addition(double firstArgument, double secondArgument) {
        log.log(Level.INFO, "for addition incoming value: " + firstArgument + "  " + secondArgument);
        if (Double.valueOf(firstArgument).equals(Double.NaN)) {
            log.log(Level.WARN, "Illegal argument: firstArgument should not be equal to NaN. Current value: " + firstArgument);
            throw new IllegalArgumentException("Illegal argument: firstArgument should not be equal to NaN.");
        }
        if (Double.valueOf(secondArgument).equals(Double.NaN)) {
            log.log(Level.WARN, "Illegal argument: secondArgument should not be equal to NaN. Current value: " + secondArgument);
            throw new IllegalArgumentException("Illegal argument: secondArgument should not be equal to NaN.");
        }
        if (firstArgument == Double.NEGATIVE_INFINITY) {
            log.log(Level.WARN, "Illegal argument: firstArgument should not be equal to Negative Infinity. Current value: " + firstArgument);
            throw new IllegalArgumentException("Illegal argument: firstArgument should not be equal to Negative Infinity.");
        }
        if (secondArgument == Double.NEGATIVE_INFINITY) {
            log.log(Level.WARN, "Illegal argument: secondArgument should not be equal to Negative Infinity. Current value: " + secondArgument);
            throw new IllegalArgumentException("Illegal argument: secondArgument should not be equal to Negative Infinity.");
        }
        if (firstArgument == Double.POSITIVE_INFINITY) {
            log.log(Level.WARN, "Illegal argument: firstArgument should not be equal to Positive Infinity. Current value: " + firstArgument);
            throw new IllegalArgumentException("Illegal argument: firstArgument should not be equal to Positive Infinity.");
        }
        if (secondArgument == Double.POSITIVE_INFINITY) {
            log.log(Level.WARN, "Illegal argument: secondArgument should not be equal to Positive Infinity. Current value: " + secondArgument);
            throw new IllegalArgumentException("Illegal argument: secondArgument should not be equal to Positive Infinity.");
        }
        if (secondArgument == 0) {
            log.log(Level.WARN, "Illegal argument: secondArgument should not be equal to zero.");
            throw new IllegalArgumentException("Illegal argument: secondArgument should not be equal to zero.");
        }
        return firstArgument + secondArgument;
    }

    /**
     * Calculating the multiplication of two numbers
     *
     * @param firstArgument  first element
     * @param secondArgument second element
     * @return multiplication
     * @throws IllegalArgumentException if one of the numbers is {@code Double.NaN} or {@code Double.Infinity}
     */
    static double multiplication(double firstArgument, double secondArgument) {
        log.log(Level.INFO, "for multiplication incoming value: " + firstArgument + "  " + secondArgument);
        if (Double.valueOf(firstArgument).equals(Double.NaN)) {
            log.log(Level.WARN, "Illegal argument: firstArgument should not be equal to NaN. Current value: " + firstArgument);
            throw new IllegalArgumentException("Illegal argument: firstArgument should not be equal to NaN.");
        }
        if (Double.valueOf(secondArgument).equals(Double.NaN)) {
            log.log(Level.WARN, "Illegal argument: secondArgument should not be equal to NaN. Current value: " + secondArgument);
            throw new IllegalArgumentException("Illegal argument: secondArgument should not be equal to NaN.");
        }
        if (firstArgument == Double.NEGATIVE_INFINITY) {
            log.log(Level.WARN, "Illegal argument: firstArgument should not be equal to Negative Infinity. Current value: " + firstArgument);
            throw new IllegalArgumentException("Illegal argument: firstArgument should not be equal to Negative Infinity.");
        }
        if (secondArgument == Double.NEGATIVE_INFINITY) {
            log.log(Level.WARN, "Illegal argument: secondArgument should not be equal to Negative Infinity. Current value: " + secondArgument);
            throw new IllegalArgumentException("Illegal argument: secondArgument should not be equal to Negative Infinity.");
        }
        if (firstArgument == Double.POSITIVE_INFINITY) {
            log.log(Level.WARN, "Illegal argument: firstArgument should not be equal to Positive Infinity. Current value: " + firstArgument);
            throw new IllegalArgumentException("Illegal argument: firstArgument should not be equal to Positive Infinity.");
        }
        if (secondArgument == Double.POSITIVE_INFINITY) {
            log.log(Level.WARN, "Illegal argument: secondArgument should not be equal to Positive Infinity. Current value: " + secondArgument);
            throw new IllegalArgumentException("Illegal argument: secondArgument should not be equal to Positive Infinity.");
        }
        if (secondArgument == 0) {
            log.log(Level.WARN, "Illegal argument: secondArgument should not be equal to zero.");
            throw new IllegalArgumentException("Illegal argument: secondArgument should not be equal to zero.");
        }
        return firstArgument * secondArgument;
    }

    /**
     * Calculating the difference of two numbers
     *
     * @param manuend    first element
     * @param subtrahend second element
     * @return difference
     * @throws IllegalArgumentException if one of the numbers is {@code Double.NaN} or {@code Double.Infinity}
     */
    static double subtraction(double manuend, double subtrahend) {
        log.log(Level.INFO, "for subtraction incoming value: " + manuend + "  " + subtrahend);
        if (Double.valueOf(manuend).equals(Double.NaN)) {
            log.log(Level.WARN, "Illegal argument: manuend should not be equal to NaN. Current value: " + manuend);
            throw new IllegalArgumentException("Illegal argument: manuend should not be equal to NaN.");
        }
        if (Double.valueOf(subtrahend).equals(Double.NaN)) {
            log.log(Level.WARN, "Illegal argument: subtrahend should not be equal to NaN. Current value: " + subtrahend);
            throw new IllegalArgumentException("Illegal argument: subtrahend should not be equal to NaN.");
        }
        if (manuend == Double.NEGATIVE_INFINITY) {
            log.log(Level.WARN, "Illegal argument: manuend should not be equal to Negative Infinity. Current value: " + manuend);
            throw new IllegalArgumentException("Illegal argument: manuend should not be equal to Negative Infinity.");
        }
        if (subtrahend == Double.NEGATIVE_INFINITY) {
            log.log(Level.WARN, "Illegal argument: subtrahend should not be equal to Negative Infinity. Current value: " + subtrahend);
            throw new IllegalArgumentException("Illegal argument: subtrahend should not be equal to Negative Infinity.");
        }
        if (manuend == Double.POSITIVE_INFINITY) {
            log.log(Level.WARN, "Illegal argument: manuend should not be equal to Positive Infinity. Current value: " + manuend);
            throw new IllegalArgumentException("Illegal argument: manuend should not be equal to Positive Infinity.");
        }
        if (subtrahend == Double.POSITIVE_INFINITY) {
            log.log(Level.WARN, "Illegal argument: subtrahend should not be equal to Positive Infinity. Current value: " + subtrahend);
            throw new IllegalArgumentException("Illegal argument: subtrahend should not be equal to Positive Infinity.");
        }
        return manuend - subtrahend;
    }

    /**
     * Calculating the ratio of two numbers
     *
     * @param dividend first element
     * @param divider  second element
     * @return ratio
     * @throws IllegalArgumentException if one of the numbers is {@code Double.NaN} or {@code Double.Infinity}
     */
    static double division(double dividend, double divider) {
        log.log(Level.INFO, "for division incoming value: " + dividend + "  " + divider);
        if (Double.valueOf(dividend).equals(Double.NaN)) {
            log.log(Level.WARN, "Illegal argument: dividend should not be equal to NaN. Current value: " + dividend);
            throw new IllegalArgumentException("Illegal argument: dividend should not be equal to NaN.");
        }
        if (Double.valueOf(divider).equals(Double.NaN)) {
            log.log(Level.WARN, "Illegal argument: divider should not be equal to NaN. Current value: " + divider);
            throw new IllegalArgumentException("Illegal argument: divider should not be equal to NaN.");
        }
        if (dividend == Double.NEGATIVE_INFINITY) {
            log.log(Level.WARN, "Illegal argument: dividend should not be equal to Negative Infinity. Current value: " + dividend);
            throw new IllegalArgumentException("Illegal argument: dividend should not be equal to Negative Infinity.");
        }
        if (divider == Double.NEGATIVE_INFINITY) {
            log.log(Level.WARN, "Illegal argument: divider should not be equal to Negative Infinity. Current value: " + divider);
            throw new IllegalArgumentException("Illegal argument: divider should not be equal to Negative Infinity.");
        }
        if (dividend == Double.POSITIVE_INFINITY) {
            log.log(Level.WARN, "Illegal argument: dividend should not be equal to Positive Infinity. Current value: " + dividend);
            throw new IllegalArgumentException("Illegal argument: dividend should not be equal to Positive Infinity.");
        }
        if (divider == Double.POSITIVE_INFINITY) {
            log.log(Level.WARN, "Illegal argument: divider should not be equal to Positive Infinity. Current value: " + divider);
            throw new IllegalArgumentException("Illegal argument: divider should not be equal to Positive Infinity.");
        }
        if (divider == 0) {
            log.log(Level.WARN, "Illegal argument: divider should not be equal to zero.");
            throw new IllegalArgumentException("Illegal argument: divider should not be equal to zero.");
        }
        return dividend / divider;
    }

    /**
     * Calculating square root
     *
     * @param argument is given number
     * @return square root
     * @throws IllegalArgumentException if number less then zero
     */
    static double sqrt(double argument) {
        log.log(Level.INFO, "for square root incoming value: " + argument);
        if (argument < 0) {
            log.log(Level.WARN, "Illegal argument: power must be more or equal zero. Current value: " + argument);
            throw new IllegalArgumentException("Wrong argument: " + argument + ". Must be more then zero.");
        }
        return Math.sqrt(argument);
    }

    /**
     * Method check number for being prime
     *
     * @param argument is given number
     * @return true if this number prime
     * @throws IllegalArgumentException if this number less then zero
     */
    static boolean isPrime(long argument) {
        log.log(Level.INFO, "for prime check incoming value: " + argument);
        if (argument <= 0) {
            log.log(Level.WARN, "Illegal argument: power must be more then zero. Current value: " + argument);
            throw new IllegalArgumentException("only Natural number can be primed");
        }
        if (argument % 2 == 0) return false;
        for (long i = 3; i < argument / 2; i = i + 2) {
            if (argument % i == 0) return false;
        }
        return true;
    }

    /**
     * Calculate number of Fibonacci of incoming power
     *
     * @param power
     * @return number of needed power
     * @throws IllegalArgumentException if power less then 0
     */
    static long numberOfFibonacci(int power) {
        log.log(Level.INFO, "serial number Fibonacci incoming value: " + power);
        if (power < 0) {
            log.log(Level.WARN, "Illegal argument: power must be more or equal zero. Current value: " + power);
            throw new IllegalArgumentException("power must be more then zero");
        }
        if (power == 0) return 0;
        if (power == 1) return 1;
        long[] fib = new long[power + 1];
        fib[0] = 0;
        fib[1] = 1;
        for (int i = 2; i < power + 1; i++) {
            fib[i] = fib[i - 1] + fib[i - 2];
        }
        return fib[power];
    }

    /**
     * Calculates sum of incoming array
     *
     * @param row incoming array
     * @return sum of the element
     */
    static int summarizeNumberRow(int[] row) {
        int sum = 0;
        for (int value : row) {
            sum = sum + value;
        }
        return sum;
    }



}
