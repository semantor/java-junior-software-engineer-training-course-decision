package com.gitlab.semantor.testing;

class NumberRowSupplier {
    /**
     * Final method for PowerMockito
     *
     * @return array {1,2,3}
     */
    final int[] finalMethod() {
        return new int[]{1, 2, 3};
    }

    /**
     * Static method for PowerMockito
     *
     * @return array {4,5,6}
     */
    static int[] staticMethod() {
        return new int[]{4, 5, 6};
    }

    /**
     * private method for PowerMockito
     *
     * @return array {7,8,9}
     */
    private int[] privateMethod() {
        return new int[]{7, 8, 9};
    }

    /**
     * package method for PowerMockito to return value of private method {@link #privateMethod()}
     *
     * @return private method array {7,8,9}
     */
    int[] publicForPrivateMethod() {
        return privateMethod();
    }


    /**
     * Good method for using Mockito
     *
     * @return array of {10,11,12}
     */
    int[] goodMethod() {
        return new int[]{10, 11, 12};
    }

    /**
     * Another good method for using Mockito
     *
     * @return array of {13,14,15}
     */
    int[] anotherGoodMethod() {
        return new int[]{13, 14, 15};
    }
}
