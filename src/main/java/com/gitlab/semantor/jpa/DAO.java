package com.gitlab.semantor.jpa;

import java.util.List;

public interface DAO {
    List createGeneralTable();

    List passengersOrderByBirthday();
}
