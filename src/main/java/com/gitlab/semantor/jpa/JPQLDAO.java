package com.gitlab.semantor.jpa;


import com.gitlab.semantor.jpa.entity.Passenger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;


public class JPQLDAO implements DAO {

    private EntityManagerFactory emf = Persistence
            .createEntityManagerFactory("NewPersistenceUnit");
    private EntityManager em = emf.createEntityManager();

    @Override
    public List<Object[]> createGeneralTable() {
        String request = "Select p .name, p .surname, " +
                "f.pointOfDeparture, f.pointOfDestination " +
                "from Passenger p " +
                "Join Ticket t " +
                "On p.ticket.id = t.id " +
                "Join Flight f " +
                "On t.flight.id = f .id " +
                "order by p.id";
        Query query = em.createQuery(request);
        List<Object[]> results = query.getResultList();
        return results;
    }

    @Override
    public List<Passenger> passengersOrderByBirthday() {   //StackOverFlow
        String request =
                "Select p from Passenger p " +
                        "order by birthday";
        Query query = em.createQuery(request);
        List<Passenger> results = query.getResultList();
        return results;
    }


}
