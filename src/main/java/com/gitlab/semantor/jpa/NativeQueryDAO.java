package com.gitlab.semantor.jpa;


import com.gitlab.semantor.jpa.entity.Passenger;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;


public class NativeQueryDAO implements DAO {

    EntityManager em = Persistence
            .createEntityManagerFactory("NewPersistenceUnit")
            .createEntityManager();

    @Override
    public List<Object[]> createGeneralTable() {
        Query query = em.createNativeQuery("select passengers.name , passengers.surname, " +
                "flights.departure, flights.destination " +
                "from passengers,tickets,flights " +
                "where tickets.flight_id = flights.flight_id and " +
                "passengers.ticket_id = tickets.ticket_id " +
                "order by passengers.id");
        List<Object[]> result = query.getResultList();
        return result;
    }

    @Override
    public List<Passenger> passengersOrderByBirthday() {
        Query query = em.createNativeQuery("select * from passengers order by birthday", Passenger.class);
        List<Passenger> result = query.getResultList();
        return result;
    }

}
