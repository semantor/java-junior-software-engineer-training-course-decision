package com.gitlab.semantor.jpa.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "tickets")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Ticket {
    @Id
    @Column(name = "ticket_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @EqualsAndHashCode.Exclude
    @Setter(AccessLevel.NONE)
    private int id;
    @Column(name = "position")
    private String position;
    @ManyToOne
    @JoinColumn(name = "flight_id")
    private Flight flight;
    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn(name = "passenger_id")
    private Passenger passenger;

    public Ticket(String position, Flight flight, Passenger passenger) {
        this.position = position;
        this.flight = flight;
        this.passenger = passenger;
    }
}
