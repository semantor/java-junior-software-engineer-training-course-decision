package com.gitlab.semantor.jpa.entity;

import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode
public class GeneralTable {
    private final List<Line> lines = new ArrayList<>();

    private GeneralTable() {
    }


    public static GeneralTable createGT(List<Object[]> income) {
        GeneralTable gt = new GeneralTable();
        income.forEach(x -> {
            gt.getLines().add(gt.createLine(x));
        });
        return gt;
    }

    private Line createLine(Object[] objects) {
        return new Line((String) objects[0],
                (String) objects[1],
                (String) objects[2],
                (String) objects[3]);

    }

    public List<Line> getLines() {
        return lines;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        lines.forEach(x -> {
            s.append(x + "\n");
        });
        return s.toString();
    }

    @EqualsAndHashCode
    public class Line {
        private String name;
        private String surname;
        private String departure;
        private String destination;


        private Line(String name, String surname, String departure, String destination) {
            this.name = name;
            this.surname = surname;
            this.departure = departure;
            this.destination = destination;
        }


        public String getName() {
            return name;
        }

        public String getSurname() {
            return surname;
        }

        public String getDeparture() {
            return departure;
        }

        public String getDestination() {
            return destination;
        }

        @Override
        public String toString() {
            return name + " " + surname + " " + departure + " " + destination;
        }
    }

}
