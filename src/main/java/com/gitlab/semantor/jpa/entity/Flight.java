package com.gitlab.semantor.jpa.entity;


import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "flights")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Flight {
    @Id
    @Column(name = "flight_id")
    @EqualsAndHashCode.Exclude
    @Setter(AccessLevel.NONE)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "departure")
    private String pointOfDeparture;
    @Column(name = "destination")
    private String pointOfDestination;
    @OneToMany(mappedBy = "flight")
    @ToString.Exclude
    private List<Ticket> tickets;

    public Flight(String pointOfDeparture, String pointOfDestination, List<Ticket> tickets) {
        this.pointOfDeparture = pointOfDeparture;
        this.pointOfDestination = pointOfDestination;
        this.tickets = tickets;
    }
}
