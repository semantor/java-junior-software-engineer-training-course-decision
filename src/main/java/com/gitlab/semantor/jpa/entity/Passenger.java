package com.gitlab.semantor.jpa.entity;


import lombok.*;

import javax.persistence.*;
import java.sql.Date;

@NamedQueries({
        @NamedQuery(name = "generalTableWithNamedQuery",
                query = "SELECT p .name, p .surname, " +
                        "f .pointOfDeparture, f.pointOfDestination " +
                        "from Passenger p, Flight  f, Ticket t " +
                        "JOIN p .ticket passengerTicket " +
                        "JOIN t .flight ticketFlight " +
                        "JOIN t.passenger ticketPassenger " +
                        "JOIN f.tickets flightTicket " +
                        "WHERE passengerTicket = ticketPassenger AND flightTicket = ticketFlight " +
                        "order by p.id")
        ,
        @NamedQuery(name = "passengersOrderByBirthdayWithNamedQuery",
                query = "select p " +
                        "FROM Passenger p " +
                        "order by birthday")})
@SqlResultSetMappings({
        @SqlResultSetMapping(
                name = "GeneralTable",
                entities = {
                        @EntityResult(
                                entityClass = Passenger.class,
                                fields = {
                                        @FieldResult(name = "name", column = "name"),
                                        @FieldResult(name = "surname", column = "surname")}),
                        @EntityResult(
                                entityClass = Flight.class,
                                fields = {
                                        @FieldResult(name = "departure", column = "departure"),
                                        @FieldResult(name = "destination", column = "destination")})})
})


@Entity
@Table(name = "passengers")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Passenger {
    @Id
    @EqualsAndHashCode.Exclude
    @Setter(AccessLevel.NONE)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "surname")
    private String surname;
    @Column(name = "birthday")
    private Date birthday;
    @OneToOne
    @JoinColumn(name = "ticket_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Ticket ticket;

    public Passenger(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Passenger(String name, String surname, Ticket ticket) {
        this.name = name;
        this.surname = surname;
        this.ticket = ticket;
    }

    public Passenger(String name, String surname, Date birthday) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
    }

    public Passenger(int id, String name, String surname, Date birthday) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
    }
}
