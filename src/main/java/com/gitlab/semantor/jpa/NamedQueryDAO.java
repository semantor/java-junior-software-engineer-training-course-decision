package com.gitlab.semantor.jpa;

import com.gitlab.semantor.jpa.entity.Passenger;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;


public class NamedQueryDAO implements DAO {

    EntityManager em = Persistence
            .createEntityManagerFactory("NewPersistenceUnit")
            .createEntityManager();

    @Override
    public List<Object[]> createGeneralTable() {
        Query query = em.createNamedQuery("generalTableWithNamedQuery");
        List<Object[]> result = query.getResultList();
        return result;
    }

    @Override
    public List<Passenger> passengersOrderByBirthday() {
        Query query = em
                .createNamedQuery("passengersOrderByBirthdayWithNamedQuery");
        List<Passenger> result = query.getResultList();
        return result;
    }

}
