package com.gitlab.semantor.jpa;

import com.gitlab.semantor.jpa.entity.Flight;
import com.gitlab.semantor.jpa.entity.Passenger;
import com.gitlab.semantor.jpa.entity.Ticket;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class CriteriaDAO implements DAO {

    private EntityManager em = Persistence
            .createEntityManagerFactory("NewPersistenceUnit")
            .createEntityManager();


    @Override
    public List<Object[]> createGeneralTable() {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Object[]> passengerCriteriaQuery = builder.createQuery(Object[].class);
        Root<Passenger> passenger = passengerCriteriaQuery.from(Passenger.class);

        Join<Passenger, Ticket> joinTicket = passenger.join("ticket");
        Join<Ticket, Flight> joinFlight = joinTicket.join("flight");
        List<Predicate> conditions = new ArrayList<>();
        conditions.add(builder.equal(passenger.get("id"), joinTicket.get("passenger")));
        conditions.add(builder.equal(joinTicket.get("flight"), joinFlight.get("id")));

        passengerCriteriaQuery
                .multiselect(passenger.get("name"),
                        passenger.get("surname"),
                        joinFlight.get("pointOfDeparture"),
                        joinFlight.get("pointOfDestination"))
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(passenger.get("id")));

        List<Object[]> result = em.createQuery(passengerCriteriaQuery).getResultList();
        return result;
    }

    @Override
    public List<Passenger> passengersOrderByBirthday() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Passenger> passengerCriteriaQuery = cb.createQuery(Passenger.class);
        Root<Passenger> passengerRoot = passengerCriteriaQuery.from(Passenger.class);
        passengerCriteriaQuery.select(passengerRoot).orderBy(cb.asc(passengerRoot.get("birthday")));

        List<Passenger> result = em.createQuery(passengerCriteriaQuery).getResultList();
        return result;
    }
}
