
package com.gitlab.semantor.xml.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class OtherServiceAndFacility implements Serializable {

    @JsonProperty("Code")
    private String code;
    @JsonProperty("Name")
    private String name;
    private final static long serialVersionUID = 2415023357632885079L;

}
