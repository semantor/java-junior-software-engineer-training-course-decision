
package com.gitlab.semantor.xml.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class Branch implements Serializable {

    @JsonProperty("Identification")
    private String identification;
    @JsonProperty("SequenceNumber")
    private String sequenceNumber;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Type")
    private String type;
    @JsonProperty("CustomerSegment")
    private List<String> customerSegment = null;
    @JsonProperty("Accessibility")
    private List<String> accessibility = null;
    @JsonProperty("OtherServiceAndFacility")
    private List<OtherServiceAndFacility> otherServiceAndFacility = null;
    @JsonProperty("Availability")
    private Availability availability;
    @JsonProperty("ContactInfo")
    private List<ContactInfo> contactInfo = null;
    @JsonProperty("PostalAddress")
    private PostalAddress postalAddress;
    private final static long serialVersionUID = 7237914469639159047L;

}
