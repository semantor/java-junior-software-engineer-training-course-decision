
package com.gitlab.semantor.xml.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)

@lombok.Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Data implements Serializable {

    @JsonProperty("data")
    private List<Datum> data = null;
    private final static long serialVersionUID = -4776267085730258139L;

}
