
package com.gitlab.semantor.xml.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class PostalAddress implements Serializable {

    @JsonProperty("AddressLine")
    private List<String> addressLine = null;
    @JsonProperty("TownName")
    private String townName;
    @JsonProperty("CountrySubDivision")
    private List<String> countrySubDivision = null;
    @JsonProperty("Country")
    private String country;
    @JsonProperty("PostCode")
    private String postCode;
    @JsonProperty("GeoLocation")
    private GeoLocation geoLocation;
    private final static long serialVersionUID = -8858632821964759109L;

}
