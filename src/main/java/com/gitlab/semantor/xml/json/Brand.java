
package com.gitlab.semantor.xml.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class Brand implements Serializable {

    @JsonProperty("BrandName")
    private String brandName;
    @JsonProperty("Branch")
    private List<Branch> branch = null;
    private final static long serialVersionUID = 7683699139183926480L;

}
