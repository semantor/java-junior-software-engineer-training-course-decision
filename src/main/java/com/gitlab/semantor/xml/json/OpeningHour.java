
package com.gitlab.semantor.xml.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OpeningHour implements Serializable {

    @JsonProperty("OpeningTime")
    private String openingTime;
    @JsonProperty("ClosingTime")
    private String closingTime;
    private final static long serialVersionUID = 637058807217044740L;

}
