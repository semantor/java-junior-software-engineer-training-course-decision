
package com.gitlab.semantor.xml.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class Availability implements Serializable {

    @JsonProperty("StandardAvailability")
    private StandardAvailability standardAvailability;
    private final static long serialVersionUID = 6922963959366347920L;

}
