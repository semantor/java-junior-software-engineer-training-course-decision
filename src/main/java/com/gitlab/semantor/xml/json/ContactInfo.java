
package com.gitlab.semantor.xml.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class ContactInfo implements Serializable {

    @JsonProperty("ContactType")
    private String contactType;
    @JsonProperty("ContactContent")
    private String contactContent;
    private final static long serialVersionUID = -5206067881636856859L;

}
