package com.gitlab.semantor.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.w3c.dom.Document;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.json.*;
import javax.xml.parsers.*;
import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.Formatter;
import java.util.Optional;

class Serializator {
    private static final ObjectMapper mapper = new ObjectMapper();


    /**
     * Actually jsr 353 doesnt include in java 12, so 374 will be import
     *
     * @param path
     */
    void readJson(Path path) {
        try (JsonReader jsonReader = Json.createReader(new FileInputStream(path.toFile()))) {
            JsonObject object = jsonReader.readObject();
            JsonValue data = object.get("data");
            JsonArray array = data.asJsonArray();
            JsonObject brand = array.getJsonObject(0);
            JsonValue brand1 = brand.get("Brand");
            Formatter f = new Formatter();
            f.format("Node with name %s has type %s and value: %s", "Brand", brand1.getValueType().toString(), brand1.toString());
            System.out.println(f);
        } catch (FileNotFoundException ex) {
            ex.toString();
        }
    }

    void serializeJSONInFile(Object data, File file) {
        try {
            mapper.writeValue(file, data);
        } catch (IOException ex) {
            ex.toString();
        }
    }

    Object deserializeJSONFromFile(Object object, File file) {
        try {
            object = mapper.readValue(file, object.getClass());
        } catch (IOException ex) {
            ex.toString();
        }
        return object;
    }

    void findNeededAttribute() {
        try {
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            SAXParser saxParser = saxParserFactory.newSAXParser();
            InputStream is =
                    new FileInputStream(new File(ClassLoader.getSystemResource("retrieveCurrentCPFFUNCP.xml").toURI()));
            DefaultHandler handler = new SAXHandler();
            saxParser.parse(is, handler);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    class SAXHandler extends DefaultHandler {
        boolean flag = false;

        @Override
        public void startElement(String uri, String localName,
                                 String qName, Attributes attributes) throws SAXException {
            int attributeLength = attributes.getLength();
            for (int i = 0; i < attributeLength; i++) {
                if (attributes.getQName(i).equals("OBS_STATUS") && attributes.getValue(i).equals("M")) {
                    flag = true;
                    return;
                }
            }
        }

        @Override
        public void endDocument() throws SAXException {
            if (!flag) throw new IllegalArgumentException("There is no argument with this value");
        }
    }

    Document createDOM() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Optional<Document> document = Optional.empty();
        try {
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            FileInputStream fis = new FileInputStream(new File(ClassLoader.getSystemResource("retrieveCurrentCPFFUNCP.xml").toURI()));
            document = Optional.of(documentBuilder.parse(fis));
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return document.get();
    }
}
