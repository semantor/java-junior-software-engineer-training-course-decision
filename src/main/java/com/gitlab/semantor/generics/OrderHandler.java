package com.gitlab.semantor.generics;

import com.gitlab.semantor.collections.Main;

interface OrderHandler<T extends Main.Order> {
    void processing(T t);
}
