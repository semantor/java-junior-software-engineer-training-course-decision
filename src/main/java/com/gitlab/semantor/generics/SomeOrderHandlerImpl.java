package com.gitlab.semantor.generics;

import com.gitlab.semantor.collections.Main;

import java.lang.reflect.Field;

class SomeOrderHandlerImpl<T extends Main.Order> implements OrderHandler<T> {

    private static SomeOrderHandlerImpl instance;

    private SomeOrderHandlerImpl() {
    }

    public static SomeOrderHandlerImpl getInstance() {
        if (instance == null) instance = new SomeOrderHandlerImpl();
        return instance;
    }

    @Override
    public void processing(T t) {
        Main.OrderStatus os = null;
        if (t.status.equals(Main.OrderStatus.NOT_STARTED)) {
            os = Main.OrderStatus.PROCESSING;
        } else if (t.status.equals(Main.OrderStatus.PROCESSING)){
            os = Main.OrderStatus.COMPLETED;
        }else return;

        try {
            Field field = t.getClass().getDeclaredField("status");
            if (!field.isAccessible()) field.setAccessible(true);
            field.set(t, os);
        } catch (NoSuchFieldException ex){
            System.out.println("Wrong field name");
        } catch (IllegalAccessException ex){
            System.out.println("some problem with accessible");
        }
    }


}
