package com.gitlab.semantor.rdb.jdbc.entities;

import lombok.*;

@AllArgsConstructor
@RequiredArgsConstructor
@ToString
public class City implements Entity {

    public static final City defaultCity = new City("name");
    public static final String tableName = "cities";
    @Getter
    private int id=0;
    @Getter @Setter @NonNull private String name;


    @Override
    public String getTableName() {
        return tableName;
    }

    public City() {
        this.name = City.defaultCity.name;
    }
}
