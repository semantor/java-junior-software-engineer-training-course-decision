package com.gitlab.semantor.rdb.jdbc.entities;

import lombok.*;

import java.sql.Date;


@RequiredArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Student implements Entity {

    public static final Student defaultStudent = new Student(0,"surname",
            "name","patronymic",Date.valueOf("1990-12-12"),"gender");

    public static final String tableName = "students";

    @Getter @EqualsAndHashCode.Exclude @ToString.Exclude
    private int id = 0;
    @Getter @Setter @NonNull private int groupID;
    @Getter @Setter @NonNull private String surname;
    @Getter @Setter @NonNull private String name;
    @Getter @Setter @NonNull private String patronymic;
    @Getter @Setter @NonNull private Date birthday;
    @Getter @Setter @NonNull private String gender;

    public Student() {
    }

    public Student(int id){
        new Student();
        this.id = id;
    }

    @Override
    public String getTableName() {
        return tableName;
    }
}
