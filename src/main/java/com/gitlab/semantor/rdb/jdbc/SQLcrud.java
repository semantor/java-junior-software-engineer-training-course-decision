package com.gitlab.semantor.rdb.jdbc;

import com.gitlab.semantor.rdb.jdbc.entities.*;

import java.sql.*;
import java.util.Optional;
import java.util.Properties;

public class SQLcrud {
    private final String DBNAME = "mega_simple_students";
    private final String URL;
    private final String DBURL;
    private final Properties properties = new Properties();

    SQLcrud(String url, String dbName, String user, String password, String booleanSSL) {
        this.URL=url;
        this.DBURL = url + "/" + dbName;
        properties.setProperty("user", user);
        properties.setProperty("password", password);
        properties.setProperty("ssl", booleanSSL);
    }

    boolean createTable(City entity) {
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            String dropTable = "create table if not exists " + entity.getTableName() +
                    "(\n" +
                    "    id   serial primary key,\n" +
                    "    name varchar not null\n" +
                    ");";
            PreparedStatement statement = connection.prepareStatement(dropTable);
            statement.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return true;
    }

    boolean createTable(University entity) {
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            String dropTable = "create table if not exists " + entity.getTableName() +
                    "(\n" +
                    "    id     serial primary key,\n" +
                    "    cityID int     not null,\n" +
                    "    name   varchar not null,\n" +
                    "    foreign key (cityID) references cities (id) on update cascade\n" +
                    ");";
            PreparedStatement statement = connection.prepareStatement(dropTable);
            statement.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return true;
    }

    boolean createTable(Group entity) {
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            String dropTable = "create table if not exists " + entity.getTableName() +
                    "(\n" +
                    "    id           serial primary key,\n" +
                    "    universityID int     not null,\n" +
                    "    name         varchar not null,\n" +
                    "    foreign key (universityID) references universities (id) on update cascade\n" +
                    "\n" +
                    ");";
            PreparedStatement statement = connection.prepareStatement(dropTable);
            statement.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return true;
    }

    boolean createTable(Student entity) {
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            String dropTable = "create table if not exists " + entity.getTableName() +
                    "(\n" +
                    "    id         serial primary key,\n" +
                    "    groupID    int     not null ,\n" +
                    "    surname    varchar not null,\n" +
                    "    name       varchar not null,\n" +
                    "    patronymic varchar not null,\n" +
                    "    birthday   date    not null,\n" +
                    "    gender     varchar not null,\n" +
                    "    foreign key (groupID) references groups (id) on update cascade\n" +
                    "\n" +
                    ");";
            PreparedStatement statement = connection.prepareStatement(dropTable);
            statement.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return true;
    }


    boolean dropTable(String name) {
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            String dropTable = "drop table " + name;
            PreparedStatement statement = connection.prepareStatement(dropTable);
            statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return true;
    }

    boolean createTestDB() {
        try (Connection connection = DriverManager.getConnection(URL, properties)) {
            String createDB = "create database " + DBNAME;
            PreparedStatement statement = connection.prepareStatement(createDB);
            statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return true;
    }


    boolean create(City city) {
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            String addGroup = "insert into cities (name) " +
                    "values (?)";
            PreparedStatement preparedStatement = connection.prepareStatement(addGroup);
            preparedStatement.setString(1, city.getName());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return true;
    }

    boolean create(Student student) {
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            String addStudent = "insert into students (groupID, surname, name, patronymic, birthday, gender) " +
                    "values (?,?,?,?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(addStudent);
            preparedStatement.setInt(1, student.getGroupID());
            preparedStatement.setString(2, student.getSurname());
            preparedStatement.setString(3, student.getName());
            preparedStatement.setString(4, student.getPatronymic());
            preparedStatement.setDate(5, student.getBirthday());
            preparedStatement.setString(6, student.getGender());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return true;
    }

    boolean create(Group group) {
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            String addGroup = "insert into groups (universityID, name) " +
                    "values (?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(addGroup);
            preparedStatement.setInt(1, group.getUniversityID());
            preparedStatement.setString(2, group.getName());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return true;
    }


    boolean create(University university) {
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            String addUniversity = "insert into universities (cityID, name) " +
                    "values (?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(addUniversity);
            preparedStatement.setInt(1, university.getCityID());
            preparedStatement.setString(2, university.getName());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return true;
    }

    boolean readAll(Student student) {
        String query = "SELECT * FROM " + student.getTableName();
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.getResultSet();
            Student entity = new Student();
            while (resultSet.next()) {
                entity.setGroupID(resultSet.getInt(Student.defaultStudent.getGroupID()));
                entity.setName(resultSet.getString(Student.defaultStudent.getName()));
                entity.setSurname(resultSet.getString(Student.defaultStudent.getSurname()));
                entity.setPatronymic(resultSet.getString(Student.defaultStudent.getPatronymic()));
                entity.setBirthday(resultSet.getDate("birthday"));
                entity.setGender(resultSet.getString(Student.defaultStudent.getGender()));
                System.out.println(entity.toString());
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return true;
    }

    boolean readAll(University university) {
        String query = "SELECT * FROM " + university.getTableName();
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.getResultSet();
            University entity = new University();
            while (resultSet.next()) {
                entity.setName(resultSet.getString(University.defaultUniversity.getName()));
                entity.setCityID(resultSet.getInt(University.defaultUniversity.getCityID()));
                System.out.println(entity.toString());
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return true;
    }

    boolean readAll(Group group) {
        String query = "SELECT * FROM " + group.getTableName();
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.getResultSet();
            Group entity = new Group();
            while (resultSet.next()) {
                entity.setName(resultSet.getString(Group.defaultGroup.getName()));
                entity.setUniversityID(resultSet.getInt(Group.defaultGroup.getUniversityID()));
                System.out.println(entity.toString());
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return true;
    }

    boolean readAll(City city) {
        String query = "SELECT * FROM " + city.getTableName();
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.getResultSet();
            City entity = new City();
            while (resultSet.next()) {
                entity.setName(resultSet.getString(City.defaultCity.getName()));
                System.out.println(entity.toString());
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return true;
    }

    Student read(Student entity) {
        String query = "SELECT * FROM students WHERE id = ?";
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, entity.getId());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            entity.setName(resultSet.getString(Student.defaultStudent.getName()));
            entity.setSurname(resultSet.getString(Student.defaultStudent.getSurname()));
            entity.setPatronymic(resultSet.getString(Student.defaultStudent.getPatronymic()));
            entity.setGender(resultSet.getString(Student.defaultStudent.getGender()));
            entity.setGroupID(resultSet.getInt("groupid"));
            entity.setBirthday(resultSet.getDate("birthday"));
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return entity;
    }

    City read(City entity) {
        String query = "SELECT * FROM " + entity.getTableName() + " WHERE id = ?";
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, entity.getId());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            entity.setName(resultSet.getString(City.defaultCity.getName()));
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return entity;
    }

    University read(University entity) {
        String query = "SELECT * FROM " + entity.getTableName() + " WHERE id = ?";
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, entity.getId());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            entity.setName(resultSet.getString(University.defaultUniversity.getName()));
            entity.setCityID(resultSet.getInt("cityid"));
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return entity;
    }

    Group read(Group entity) {
        String query = "SELECT * FROM " + entity.getTableName() + " WHERE id = ?";
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, entity.getId());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            entity.setName(resultSet.getString(Group.defaultGroup.getName()));
            entity.setUniversityID(resultSet.getInt("universityid"));
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return entity;
    }

    boolean update(Student student) {
        if (student.getId() == 0) {
            throw new IllegalArgumentException("wrong id");
        }
        String query = "UPDATE students SET groupid=?, name=?, surname=?, patronymic=?, birthday=?, gender=? WHERE id=?";
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, student.getGroupID());
            statement.setString(2, student.getName());
            statement.setString(3, student.getSurname());
            statement.setString(4, student.getPatronymic());
            statement.setDate(5, student.getBirthday());
            statement.setString(6, student.getGender());

            statement.setInt(7, student.getId());
            statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return true;
    }

    boolean update(Group group) {
        if (group.getId() == 0) {
            throw new IllegalArgumentException("wrong id");
        }
        String query = "UPDATE groups SET universityid=?,name=? WHERE id=?";
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, group.getUniversityID());
            statement.setString(2, group.getName());
            statement.setInt(3, group.getId());
            statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return true;
    }

    boolean update(University university) {
        if (university.getId() == 0) {
            throw new IllegalArgumentException("wrong id");
        }
        String query = "UPDATE universities SET cityid=?, name=? WHERE id=?";
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, university.getCityID());
            statement.setString(2, university.getName());

            statement.setInt(3, university.getId());
            statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return true;
    }

    boolean update(City city) {
        if (city.getId() == 0) {
            throw new IllegalArgumentException("wrong id");
        }
        String query = "UPDATE cities SET name=? WHERE id=?";
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, city.getName());
            statement.setInt(2, city.getId());
            statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return true;
    }

    boolean remove(Entity entity) {
        String query = "DELETE from " + entity.getTableName() + " WHERE id=?";
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, entity.getId());
            statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return true;
    }

    void createFunction() {
        String query = "create or replace function show_the_younger()\n" +
                "    returns table\n" +
                "            (\n" +
                "                id      int,\n" +
                "                name    varchar,\n" +
                "                surname varchar\n" +
                "            )\n" +
                "\n" +
                "as\n" +
                "$$\n" +
                "select id, name, surname\n" +
                "from students\n" +
                "where birthday > date '1990-01-01'\n" +
                "$$ language sql;";
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
    }

    String useFunction() {
        String query = "select *\n" +
                "                from show_the_younger();";
        String s = "";
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            CallableStatement statement = connection.prepareCall(query);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString(Student.defaultStudent.getName());
                String surname = resultSet.getString(Student.defaultStudent.getSurname());
                s = s + id + " " + name + " " + surname + "\n";
                System.out.println(id + " " + name + " " + surname);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return s;
    }

    void dropFunction() {
        String query = "drop function show_the_younger()";
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
    }

    void createTrigger() {
        String query =
                "create or replace function check_student_for_gender() returns trigger as\n" +
                        "$cua$\n" +
                        "Begin\n" +
                        "    if old.gender = 'female' then\n" +
                        "        raise exception 'female cannot be deleted';\n" +
                        "    end if;\n" +
                        "    return old;\n" +
                        "end ;\n" +
                        "$cua$\n" +
                        "    language plpgsql;\n" +
                        "\n" +
                        "create trigger on_delete_user\n" +
                        "    before delete\n" +
                        "    on students\n" +
                        "    for each row\n" +
                        "execute procedure check_student_for_gender();";
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
    }

    void dropTrigger() {
        String query = "drop function check_student_for_gender();";
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
    }

    Date getMinAge() {
        Optional<Date> date = Optional.empty();
        String query = "select max(birthday) AS birthday from students";
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            date = Optional.of(resultSet.getDate("birthday"));
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return date.get();
    }

    Date getMaxAge() {
        Optional<Date> date = Optional.empty();
        String query = "select min(birthday) AS birthday from students";
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            date = Optional.of(resultSet.getDate("birthday"));

        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return date.get();
    }

    String createStudentsGroupTable() {
        String query = "select groups.name as group_name, s.name as student_name , s.surname " +
                "from groups inner join students s on groups.id = s.groupid where s.id = 17";
        String s = "";
        try (Connection connection = DriverManager.getConnection(DBURL, properties)) {
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            s = resultSet.getString("group_name") + " " +
                    resultSet.getString("student_name") + " " +
                    resultSet.getString("surname");
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return s;
    }
}
