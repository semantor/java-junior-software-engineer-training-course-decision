package com.gitlab.semantor.rdb.jdbc.entities;

import lombok.*;

@AllArgsConstructor
@RequiredArgsConstructor
@ToString
public class Group implements Entity {
    public static final Group defaultGroup = new Group(0,"name");
    public static final String tableName = "groups";
    @Getter private int id=0;
    @Getter @Setter @NonNull private int universityID;
    @Getter @Setter @NonNull private String name;


    @Override
    public String getTableName() {
        return tableName;
    }

    public Group() {
        this.name = Group.defaultGroup.name;
    }
}
