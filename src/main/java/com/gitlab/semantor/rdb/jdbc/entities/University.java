package com.gitlab.semantor.rdb.jdbc.entities;

import lombok.*;

@RequiredArgsConstructor
@AllArgsConstructor
@ToString
public class University implements Entity {

    public static final University defaultUniversity = new University(0,"name");
    public static final String tableName = "universities";

    @Getter private int id=0;
    @Getter @Setter @NonNull private int cityID;
    @Getter @Setter @NonNull private String name;
    @Override
    public String getTableName() {
        return tableName;
    }

    public University() {
        this.name = University.defaultUniversity.name;
        this.cityID = University.defaultUniversity.cityID;
    }
}
