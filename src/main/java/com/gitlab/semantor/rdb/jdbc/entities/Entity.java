package com.gitlab.semantor.rdb.jdbc.entities;

public interface Entity {

    String getTableName();
    String getName();
    int getId();
}
