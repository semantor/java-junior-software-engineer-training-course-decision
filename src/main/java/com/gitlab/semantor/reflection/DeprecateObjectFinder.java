package com.gitlab.semantor.reflection;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

import java.lang.annotation.Annotation;
import java.util.*;

class DeprecateObjectFinder {

    DeprecateObjectFinder() {
    }

    List<Object> searchDeprecateObject(List<Object> objects) {
        boolean b = false;
        List<Object> deprecateObjects = new ArrayList<>();
        for (Object object :
                objects) {
            b = false;
            for (Annotation annotation :
                    object.getClass().getDeclaredAnnotations()) {
                if (annotation.annotationType().getName().equals("java.lang.Deprecated")) {
                    b = true;
                    break;
                }
            }
            if (b) deprecateObjects.add(object);
        }
        return deprecateObjects;
    }

    List<Object> searchAndSuggestDeprecateObject(List<Object> objects) {
        boolean b = false;
        List<Object> deprecateObjects = new ArrayList<>();
        for (Object object :
                objects) {
            b = false;
            for (Annotation annotation :
                    object.getClass().getDeclaredAnnotations()) {
                if (annotation.annotationType().getName().equals("java.lang.Deprecated")) {
                    b = true;
                    break;
                }
            }
            if (b) {
                deprecateObjects.add(object);
                List<Class> classes = searchParentClass(object);
                Optional<Class> clazz;
                if (!classes.isEmpty()) {
                    if ((clazz = searchFirstInheritor(classes)).isPresent()) {
                        System.out.println(clazz.get().getSimpleName());
                    }
                }
            }
        }
        return deprecateObjects;
    }

    List<Class> searchParentClass(Object object) {
        List<Class> superclasses = new ArrayList<>();
        if ((object.getClass().getSuperclass()) != null) {
            superclasses.add(object.getClass().getSuperclass());
        }
        superclasses.addAll(Arrays.asList(object.getClass().getInterfaces()));
        if (superclasses.isEmpty()) System.out.println("No parents, an orphan");
        return superclasses;
    }

    Optional<Class> searchFirstInheritor(List<Class> classes) {
        for (Class clazz :
                classes) {
            Set<Class<?>> inheritors = searchAllInherit(clazz);
            for (Class aClass :
                    inheritors) {
                if (!isDepricated(aClass)) {
                    return Optional.of(aClass);
                }
            }
        }
        return Optional.empty();
    }

    Set<Class<?>> searchAllInherit(Class parentClass) {
        Package pack;
        pack = parentClass.getPackage();
        Reflections reflections = new Reflections(pack, new SubTypesScanner());
        Set<Class<?>> subTypes = new HashSet<>();
        for (String className : reflections.getStore().get(SubTypesScanner.class.getSimpleName()).values()) {
            try {
                Class subType = Class.forName(className);
                if (parentClass.isAssignableFrom(subType)) {
                    subTypes.add(subType);
                }
            } catch (ClassNotFoundException e) {
                throw new RuntimeException("This is impossible", e);
            }
        }
        return subTypes;
    }

    boolean isDepricated(Class clazz) {
        for (Annotation annotation :
                clazz.getDeclaredAnnotations()) {
            if (annotation.annotationType().getName().equals("java.lang.Deprecated")) {
                return true;
            }
        }
        return false;
    }


}