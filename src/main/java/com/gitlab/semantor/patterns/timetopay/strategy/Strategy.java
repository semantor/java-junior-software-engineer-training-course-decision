package com.gitlab.semantor.patterns.timetopay.strategy;

public interface Strategy {

    float execute();
}