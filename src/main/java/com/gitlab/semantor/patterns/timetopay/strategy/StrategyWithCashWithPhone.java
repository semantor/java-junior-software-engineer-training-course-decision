package com.gitlab.semantor.patterns.timetopay.strategy;

import com.gitlab.semantor.patterns.timetopay.BonusCache;
import lombok.AllArgsConstructor;

import java.util.Optional;

@AllArgsConstructor
public class StrategyWithCashWithPhone implements Strategy {

    private Optional<BonusCache> bonusProgram;
    private int phoneNumber;
    private float price;
    private float cash;

    @Override
    public float execute() {
        if (bonusProgram.isPresent()) {
            float bonus = bonusProgram.get().get(phoneNumber);
            if (price > bonus + cash) throw new IllegalArgumentException(" Not enough money");
            if (price <= bonus) {
                System.out.println("Full payment with bonus");
                bonusProgram.get().update(phoneNumber, bonus - price);
                return 0;
            }
            price = price - bonus;
            bonusProgram.get().update(phoneNumber, (float) (price * 0.05));
            float balance = cash - price;
            System.out.println("Cash payment with Bonus system, " + bonus + " used, " + (price * 0.05) + " added, " +
                    price + " debited" + ", " + balance + " return");
            return balance;
        } else throw new IllegalStateException("Some error in bonus system occurred");
    }
}