package com.gitlab.semantor.patterns.timetopay.strategy;

public class StrategyWithCardWithoutPhone implements Strategy {

    @Override
    public float execute() {
        //some logic to use card for payment
        System.out.println("Card payment without Bonus system");
        return 0;
    }
}