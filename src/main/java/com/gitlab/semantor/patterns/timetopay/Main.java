package com.gitlab.semantor.patterns.timetopay;

class Main {


    public static void main(String[] args) {
        BonusCache bonusProgram = new BonusCache();  //Virtual Proxy
        Payment.setBonusProgram(bonusProgram);      // Easy to exclude
        Payment.Builder.payBuilder().cardPayment(false).setBonusPhoneNumber(123).setPrice(1200).deposit(1400).build().pay();
        Payment.Builder.payBuilder().cardPayment(false).setBonusPhoneNumber(123).setPrice(1200).deposit(1400).build().pay();
        Payment.Builder.payBuilder().cardPayment(true).setBonusPhoneNumber(123).setPrice(1200).deposit(1400).build().pay();
        Payment.Builder.payBuilder().cardPayment(true).setBonusPhoneNumber(123).setPrice(1200).deposit(1400).build().pay();
        Payment.Builder.payBuilder().cardPayment(true).setPrice(1200).deposit(1400).build().pay();

    }
}
