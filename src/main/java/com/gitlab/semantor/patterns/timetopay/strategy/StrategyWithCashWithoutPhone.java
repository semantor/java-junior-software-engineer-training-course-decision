package com.gitlab.semantor.patterns.timetopay.strategy;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class StrategyWithCashWithoutPhone implements Strategy {
    private float cash;
    private float price;

    @Override
    public float execute() {
        if (price > cash) throw new IllegalArgumentException(" Not enough money");
        float balance = cash - price;
        System.out.println("Cash payment without Bonus system " + balance + " return");
        return balance;
    }
}