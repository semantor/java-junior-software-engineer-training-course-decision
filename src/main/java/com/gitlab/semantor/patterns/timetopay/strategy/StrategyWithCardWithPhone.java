package com.gitlab.semantor.patterns.timetopay.strategy;

import com.gitlab.semantor.patterns.timetopay.BonusCache;
import lombok.AllArgsConstructor;

import java.util.Optional;

@AllArgsConstructor
public class StrategyWithCardWithPhone implements Strategy {

    private Optional<BonusCache> bonusProgram;
    private int phoneNumber;
    private float price;

    @Override
    public float execute() {
        if (bonusProgram.isPresent()) {
            float bonus = bonusProgram.get().get(phoneNumber);
            if (price <= bonus) {
                System.out.println("Full payment with bonus");
                bonusProgram.get().update(phoneNumber, bonus - price);
                return 0;
            }
            price = price - bonus;
            // use money from card
            bonusProgram.get().update(phoneNumber, (float) (price * 0.05));
            System.out.println("Card payment with Bonus system, " + bonus + " used, " + (price * 0.05) + " added, " + price + " debited");
        } else throw new IllegalStateException("Some error in bonus system occurred");
        return 0;
    }
}