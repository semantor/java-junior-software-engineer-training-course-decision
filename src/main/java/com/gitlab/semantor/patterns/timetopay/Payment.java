package com.gitlab.semantor.patterns.timetopay;

import com.gitlab.semantor.patterns.timetopay.strategy.*;

import java.util.Optional;

/**
 * Builder, strategy.
 */
class Payment {
    private float price;
    private int phoneNumber;
    private boolean cardPayment;
    private float cash;
    private Strategy strategy;
    private static Optional<BonusCache> bonusProgram = Optional.empty();

    Payment(Builder builder) {
        price = builder.price;
        this.phoneNumber = builder.phoneNumber;
        this.cardPayment = builder.cardPayment;
        this.cash = builder.cash;
    }


    /**
     * Strategy!
     *
     * @return 0 with card payment or balance for cash payment.
     */
    public float pay() {
        if (cash == 0 && (phoneNumber == 0 | bonusProgram.isEmpty())) strategy = new StrategyWithCardWithoutPhone();
        if (cash != 0 && (phoneNumber == 0 | bonusProgram.isEmpty()))
            strategy = new StrategyWithCashWithoutPhone(cash, price);
        if (cash == 0 && phoneNumber != 0 && bonusProgram.isPresent())
            strategy = new StrategyWithCardWithPhone(bonusProgram, phoneNumber, price);
        if (cash != 0 && phoneNumber != 0 && bonusProgram.isPresent())
            strategy = new StrategyWithCashWithPhone(bonusProgram, phoneNumber, price, cash);
        return strategy.execute();
    }

    static void setBonusProgram(BonusCache bonus) {
        bonusProgram = Optional.of(bonus);
    }

    static class Builder {
        private float price;
        private int phoneNumber;
        private boolean cardPayment;
        private float cash;

        private Builder() {
            price = 0;
            phoneNumber = 0;
            cardPayment = false;
            cash = 0;
        }

        static Builder payBuilder() {
            return new Builder();
        }

        Builder setPrice(float price) {
            this.price = price;
            return this;
        }

        Builder cardPayment(boolean bool) {
            this.cardPayment = bool;
            return this;
        }

        Builder setBonusPhoneNumber(int phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        Builder deposit(float money) {
            cash = money;
            return this;
        }

        Payment build() {
            if (price == 0) throw new IllegalArgumentException("Incorrect price");
            if (!cardPayment && cash == 0)
                throw new IllegalStateException("if u want to pay with cash, enter your cash");
            return new Payment(this);
        }

    }
}
