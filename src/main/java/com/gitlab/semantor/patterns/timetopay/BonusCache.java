package com.gitlab.semantor.patterns.timetopay;

import com.gitlab.semantor.functional.NewCache;

public class BonusCache extends NewCache<Integer, Float> {


    @Override
    public Float getDefaultValue() {
        return (float) 0;
    }
}