package com.gitlab.semantor.patterns.pizza.command;

import com.gitlab.semantor.patterns.pizza.consumed.Consumed;
import com.gitlab.semantor.patterns.pizza.producers.FoodMaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Command implementation
 */
@Component
@Qualifier("commandToMakeDrink")
class CommandToMakeDrink implements Command {
    private final FoodMaker foodMaker;

    @Autowired
    CommandToMakeDrink(@Qualifier("Bar") FoodMaker foodMaker) {
        this.foodMaker = foodMaker;
    }

    @Override
    public Consumed execute(String name) {
        return foodMaker.make(name);
    }
}
