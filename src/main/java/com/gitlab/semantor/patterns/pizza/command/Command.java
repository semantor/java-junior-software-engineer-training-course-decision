package com.gitlab.semantor.patterns.pizza.command;

import com.gitlab.semantor.patterns.pizza.consumed.Consumed;

/**
 * Command interface
 */
public interface Command {

    Consumed execute(String name);

}
