package com.gitlab.semantor.patterns.pizza;

import com.gitlab.semantor.patterns.pizza.command.Command;
import com.gitlab.semantor.patterns.pizza.consumed.Consumed;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("waiter")
@AllArgsConstructor
class Waiter {
    private Command commandToMakeFood;
    private Command commandToMakeDrink;

    Consumed makeFood(String name) {
        return commandToMakeFood.execute(name);
    }

    Consumed makeDrink(String name) {
        return commandToMakeDrink.execute(name);
    }
}
