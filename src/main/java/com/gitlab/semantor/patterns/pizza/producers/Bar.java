package com.gitlab.semantor.patterns.pizza.producers;

import com.gitlab.semantor.patterns.pizza.consumed.Consumed;
import com.gitlab.semantor.patterns.pizza.consumed.Drink;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Receiver in Command Pattern
 */
@Component
@Qualifier("Bar")
@NoArgsConstructor
class Bar implements FoodMaker {
    private final List<String> menu = new ArrayList<>();

    @PostConstruct
    private void init() {
        menu.add("Cola");
        menu.add("Water");
        menu.add("MilkShake");
        menu.add("Vodka");
    }

    @Override
    public Consumed make(String name) {
        if (!menu.contains(name)) throw new IllegalArgumentException("no drink in bar");
        return makeWater(name);
    }

    private Consumed makeWater(String name) {
        System.out.println("Prepare drink " + name);
        return new Drink(name);
    }


}
