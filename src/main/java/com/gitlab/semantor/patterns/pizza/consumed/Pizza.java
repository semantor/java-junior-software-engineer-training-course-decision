package com.gitlab.semantor.patterns.pizza.consumed;

import lombok.Data;

@Data
public final class Pizza implements Consumed {
    private final String name;

}
