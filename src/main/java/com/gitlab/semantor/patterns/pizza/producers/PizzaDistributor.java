package com.gitlab.semantor.patterns.pizza.producers;

import com.gitlab.semantor.patterns.pizza.consumed.Pizza;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Single interface for all pizza makers
 * Facade
 */
@Component
@Qualifier("pizzadistributor")
@AllArgsConstructor
class PizzaDistributor implements FoodMaker {
    private final PizzaMaker pizzaMaker;
    private final PizzaShop pizzaShop;


    @Override
    public Pizza make(String name) {
        Pizza pizza;
        if (pizzaMaker.canCookPizza(name)) pizza = pizzaMaker.cookPizza(name);
        else pizza = pizzaShop.buyPizza(name);
        return pizza;
    }

}
