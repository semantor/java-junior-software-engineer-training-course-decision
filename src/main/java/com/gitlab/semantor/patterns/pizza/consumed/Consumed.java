package com.gitlab.semantor.patterns.pizza.consumed;

/**
 * Prepared food
 */
public interface Consumed {
    String getName();
}
