package com.gitlab.semantor.patterns.pizza.producers;

import com.gitlab.semantor.patterns.pizza.consumed.Pizza;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
@Qualifier("pizzaMaker")
@NoArgsConstructor
class PizzaMaker {
    private final List<String> menu = new ArrayList<>();

    @PostConstruct
    private void init() {
        menu.add("4 cheese");
        menu.add("Margarita");
        menu.add("Diablo");
    }

    Pizza cookPizza(String name) {
        if (menu.contains(name)) {
            System.out.println("Pizza " + name + " is cooked ");
            return new Pizza(name);
        } else throw new IllegalArgumentException("this Cook is qualified to do this");
    }

    boolean canCookPizza(String name) {
        return menu.contains(name);
    }
}
