package com.gitlab.semantor.patterns.pizza;

import com.gitlab.semantor.patterns.pizza.consumed.Consumed;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * List of patterns : Command, Facade, Singleton via SpringContext.
 */
class Main {


    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
        Waiter waiter = ctx.getBean(Waiter.class);
        String drink = "Vodka";
        String pizza = "Margarita";
        Consumed margarita = waiter.makeFood(pizza);
        Consumed vodka = waiter.makeDrink(drink);
        System.out.println("waiter bring to client " + margarita.getName() + " and " + vodka.getName());
    }


}
