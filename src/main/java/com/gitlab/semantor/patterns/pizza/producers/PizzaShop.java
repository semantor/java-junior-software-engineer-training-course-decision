package com.gitlab.semantor.patterns.pizza.producers;

import com.gitlab.semantor.patterns.pizza.consumed.Pizza;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Qualifier("pizzaShop")
@NoArgsConstructor
class PizzaShop {
    Pizza buyPizza(String name) {
        System.out.println("Pizza is bought " + name);
        return new Pizza(name);
    }
}
