package com.gitlab.semantor.patterns.pizza.producers;

import com.gitlab.semantor.patterns.pizza.consumed.Consumed;

/**
 * Interface for food makers
 */
public interface FoodMaker<T extends Consumed> {

    T make(String consumedOrder);

}
