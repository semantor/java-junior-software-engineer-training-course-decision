package com.gitlab.semantor.patterns.pizzawithoutspring.producers;

import com.gitlab.semantor.patterns.pizzawithoutspring.consumed.Pizza;

import java.util.ArrayList;
import java.util.List;

class PizzaMaker {
    private final List<String> menu = new ArrayList<>();

    PizzaMaker() {
        menu.add("4 cheese");
        menu.add("Margarita");
        menu.add("Diablo");
    }

    Pizza cookPizza(String name) {
        if (menu.contains(name)) {
            System.out.println("Pizza " + name + " is cooked ");
            return new Pizza(name);
        } else throw new IllegalArgumentException("this Cook is qualified to do this");
    }

    boolean canCookPizza(String name) {
        return menu.contains(name);
    }
}
