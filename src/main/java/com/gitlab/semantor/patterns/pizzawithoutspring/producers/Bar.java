package com.gitlab.semantor.patterns.pizzawithoutspring.producers;

import com.gitlab.semantor.patterns.pizzawithoutspring.consumed.Consumed;
import com.gitlab.semantor.patterns.pizzawithoutspring.consumed.Drink;

import java.util.ArrayList;
import java.util.List;

/**
 * Receiver in Command Pattern
 */

public class Bar implements FoodMaker {
    private final List<String> menu = new ArrayList<>();


    public Bar() {
        menu.add("Cola");
        menu.add("Water");
        menu.add("MilkShake");
        menu.add("Vodka");
    }

    @Override
    public Consumed make(String name) {
        if (!menu.contains(name)) throw new IllegalArgumentException("no drink in bar");
        return makeWater(name);
    }

    private Consumed makeWater(String name) {
        System.out.println("Prepare drink " + name);
        return new Drink(name);
    }


}
