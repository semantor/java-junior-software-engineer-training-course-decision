package com.gitlab.semantor.patterns.pizzawithoutspring.command;


import com.gitlab.semantor.patterns.pizzawithoutspring.consumed.Consumed;
import com.gitlab.semantor.patterns.pizzawithoutspring.producers.FoodMaker;
import com.gitlab.semantor.patterns.pizzawithoutspring.producers.PizzaDistributor;

public class CommandToMakeFood implements Command {
    private final FoodMaker foodMaker;

    public CommandToMakeFood() {
        this.foodMaker = new PizzaDistributor();
    }

    @Override
    public Consumed execute(String name) {
        return foodMaker.make(name);
    }

}
