package com.gitlab.semantor.patterns.pizzawithoutspring.consumed;

/**
 * Prepared food
 */
public interface Consumed {
    String getName();
}
