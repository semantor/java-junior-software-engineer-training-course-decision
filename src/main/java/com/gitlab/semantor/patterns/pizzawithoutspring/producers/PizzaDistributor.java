package com.gitlab.semantor.patterns.pizzawithoutspring.producers;

import com.gitlab.semantor.patterns.pizzawithoutspring.consumed.Pizza;

/**
 * Single interface for all pizza makers
 * Facade
 */
public class PizzaDistributor implements FoodMaker {
    private final PizzaMaker pizzaMaker;
    private final PizzaShop pizzaShop;

    public PizzaDistributor() {
        this.pizzaMaker = new PizzaMaker();
        this.pizzaShop = new PizzaShop();
    }

    @Override
    public Pizza make(String name) {
        Pizza pizza;
        if (pizzaMaker.canCookPizza(name)) pizza = pizzaMaker.cookPizza(name);
        else pizza = pizzaShop.buyPizza(name);
        return pizza;
    }

}
