package com.gitlab.semantor.patterns.pizzawithoutspring.consumed;

import lombok.Data;

@Data
public final class Drink implements Consumed {
    private final String name;
}
