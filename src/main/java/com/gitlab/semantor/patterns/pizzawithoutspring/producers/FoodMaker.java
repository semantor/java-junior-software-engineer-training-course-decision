package com.gitlab.semantor.patterns.pizzawithoutspring.producers;

import com.gitlab.semantor.patterns.pizzawithoutspring.consumed.Consumed;

/**
 * Interface for food makers
 */
public interface FoodMaker<T extends Consumed> {

    T make(String consumedOrder);

}
