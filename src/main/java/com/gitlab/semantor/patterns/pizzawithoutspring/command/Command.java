package com.gitlab.semantor.patterns.pizzawithoutspring.command;

import com.gitlab.semantor.patterns.pizzawithoutspring.consumed.Consumed;

/**
 * Command interface
 */
public interface Command {

    Consumed execute(String name);

}
