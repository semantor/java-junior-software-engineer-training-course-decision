package com.gitlab.semantor.patterns.pizzawithoutspring;

import com.gitlab.semantor.patterns.pizzawithoutspring.command.CommandToMakeDrink;
import com.gitlab.semantor.patterns.pizzawithoutspring.command.CommandToMakeFood;
import com.gitlab.semantor.patterns.pizzawithoutspring.consumed.Consumed;


class Waiter {


    Waiter() {
    }

    Consumed makeFood(String name) {
        return new CommandToMakeFood().execute(name);
    }

    Consumed makeDrink(String name) {
        return new CommandToMakeDrink().execute(name);
    }
}
