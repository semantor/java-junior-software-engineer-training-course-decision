package com.gitlab.semantor.patterns.pizzawithoutspring.producers;

import com.gitlab.semantor.patterns.pizzawithoutspring.consumed.Pizza;
import lombok.NoArgsConstructor;

@NoArgsConstructor
class PizzaShop {
    Pizza buyPizza(String name) {
        System.out.println("Pizza is bought " + name);
        return new Pizza(name);
    }
}
