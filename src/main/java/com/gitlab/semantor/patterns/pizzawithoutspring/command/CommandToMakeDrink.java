package com.gitlab.semantor.patterns.pizzawithoutspring.command;

import com.gitlab.semantor.patterns.pizzawithoutspring.consumed.Consumed;
import com.gitlab.semantor.patterns.pizzawithoutspring.producers.Bar;
import com.gitlab.semantor.patterns.pizzawithoutspring.producers.FoodMaker;

/**
 * Command implementation
 */

public class CommandToMakeDrink implements Command {
    private final FoodMaker foodMaker;


    public CommandToMakeDrink() {
        this.foodMaker = new Bar();
    }

    @Override
    public Consumed execute(String name) {
        return foodMaker.make(name);
    }
}
