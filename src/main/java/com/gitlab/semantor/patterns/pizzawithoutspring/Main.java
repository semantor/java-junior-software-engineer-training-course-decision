package com.gitlab.semantor.patterns.pizzawithoutspring;

import com.gitlab.semantor.patterns.pizzawithoutspring.consumed.Consumed;

/**
 * List of patterns : Command, Facade, Singleton via SpringContext.
 */
class Main {


    public static void main(String[] args) {
        Waiter waiter = new Waiter();
        String drink = "Vodka";
        String pizza = "Margarita";
        Consumed margarita = waiter.makeFood(pizza);
        Consumed vodka = waiter.makeDrink(drink);
        System.out.println("waiter bring to client " + margarita.getName() + " and " + vodka.getName());
    }


}
