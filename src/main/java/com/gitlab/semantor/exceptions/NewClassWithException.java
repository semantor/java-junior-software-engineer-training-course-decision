package com.gitlab.semantor.exceptions;

class NewClassWithException {
    private String field1;
    private String field2;

    NewClassWithException(String field1, String field2) {
        this.field1 = field1;
        if (field2 == null) throw new IllegalArgumentException("field2 must nonnull!");
        this.field2 = field2;
        throw new IllegalStateException("some unexpected exception");
    }

    String getField1() {
        return field1;
    }

    String getField2() {
        return field2;
    }


    public static void main(String[] args) {
        NewClassWithException newClass = null;
        try {
            newClass = new NewClassWithException("", null);
        } catch (Exception ex) {
            System.out.println(ex.toString());
        } finally {
            System.out.println(newClass.getField1());
            System.out.println(newClass.getField2());
        }
    }
}
