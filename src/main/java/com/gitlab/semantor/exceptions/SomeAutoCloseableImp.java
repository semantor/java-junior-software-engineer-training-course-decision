package com.gitlab.semantor.exceptions;

class SomeAutoCloseableImp implements AutoCloseable {

    private volatile boolean close = false;

    @Override
    public void close() throws Exception {
        if (close) return;
        //some logic to close autoclosable
        close = true;
        System.out.println(close);
    }

    synchronized void readData() {
        close = false;
        //do something
        System.out.println("Data reading");
    }

    public static void main(String[] args) {
        try (SomeAutoCloseableImp someAutoCloseableImp = new SomeAutoCloseableImp()) {
            someAutoCloseableImp.readData();
        } catch (Exception ex) {
            ex.toString();
        }
    }
}
