package com.gitlab.semantor.functional;

import com.gitlab.semantor.collections.Main;
import com.gitlab.semantor.collections.Main2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class NewOrder extends Main2.Order {

    public NewOrder(Main.OrderStatus status) {
        super(status);
    }

    public NewOrder(Main.OrderStatus status, int number) {
        super(status, number);
    }

    boolean lessThanInOrder(int number) {
        return number < getNumericField();
    }

    public static void main(String[] args) {
        List<NewOrder> orderList = new ArrayList<>();
        orderList = Stream.generate(() -> new NewOrder(Main.OrderStatus.COMPLETED, (int) (Math.random() * 100)))
                .filter(x -> x.lessThanInOrder(50)).limit(100).collect(toList());
    }
}
