package com.gitlab.semantor.functional;

import com.gitlab.semantor.collections.Main;

interface OrderFactory<T extends Main.Order> {

    default T createNotStartedOrder() {
        return createOrder(Main.OrderStatus.NOT_STARTED);
    }

    default Main.Order createProcessingOrder() {
        return createOrder(Main.OrderStatus.PROCESSING);
    }

    default Main.Order createCompletedOrder() {
        return createOrder(Main.OrderStatus.COMPLETED);
    }

    T createOrder(Main.OrderStatus status);
}
