package com.gitlab.semantor.functional;

import com.gitlab.semantor.collections.Cache;

import java.util.*;

public abstract class NewCache<K, V> extends Cache<K,V> {

    private final Map<K, Optional<V>> cache = new HashMap<>();
    private final DAO dao;

    public NewCache() {
        dao = new SpecificDAO();
    }

    public NewCache(DAO dao) {this.dao = dao;}

    public boolean contains(K k){
        if (cache.containsKey(k)) return true;
        if (dao.get(k).isPresent()) {cache.put(k,dao.get(k));return true;}
        return false;
    }

    public abstract V getDefaultValue();

    public V get(K k) {
        if (cache.get(k) != null) return cache.get(k).get();
        if (dao.get(k).isPresent()) {
            cache.put(k, dao.get(k));
            return cache.get(k).get();
        }
        dao.add(k,getDefaultValue());
        cache.put(k,Optional.of(getDefaultValue()));
        return cache.get(k).get();
    }

    public V remove(K k) {
        return cache.remove(k).get();
    }

    public void update(K k, V v) {
        if (dao.update(v))
            cache.put(k, Optional.of(v));
    }

    public boolean cleanup() {
        cache.clear();
        return true;
    }

    private interface DAO<K, T> {
        Optional<T> get(K k);

        List<T> getAll();

        void add(K k,T t);

        boolean update(T t);

        void delete(T t);
    }

    private class SpecificDAO<K, C> implements DAO<K, C> {
        @Override
        public Optional<C> get(K k) {
            //Some method get data from db
            return Optional.empty();
        }

        @Override
        public List<C> getAll() {
            //Method to get all data
            return Collections.EMPTY_LIST;
        }

        @Override
        public void add(K k, C c) {
            //add new data to db
        }

        @Override
        public boolean update(final C c) {
            //update data via id in object c
            return true;
        }

        @Override
        public void delete(final C c) {
            //delete data from db
        }
    }
}
