package com.gitlab.semantor.multithreading.bruteforce;

import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Employee implements Runnable {

    private final String targetMD5;
    private final int[] startingChar;
    private final int passwordMaxLength;
    private MessageDigest msg;

    /**
     * min symbols is `
     * Its change to space during logic
     * max symbol is z
     */
    private static final int min = 96;
    private static final int max = 122;
    private static final int amountOfSymbolsInPassword = 26;

    private volatile boolean foundPass = false;
    private volatile boolean interrupt = false;


    private Employee(String targetMD5, int[] startingChar, int passwordMaxLength) {
        this.targetMD5 = targetMD5.toLowerCase();
        this.startingChar = startingChar;
        this.passwordMaxLength = passwordMaxLength;
        try {
            msg = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
        }
    }

    /**
     * Creating a pool of Runnable Employees.
     *
     * @param amountOfEmployees amount of employees. currently optimized for max = 26
     * @param targetMD5         targetMD5 we would like to search
     * @param passwordMaxLength it must be only less then infinity.
     * @return List of employees.
     * @throws IllegalArgumentException if #amountOfEmployees > 26
     */
    static List<Employee> getEmployees(int amountOfEmployees, String targetMD5, int passwordMaxLength) {
        System.out.println("Creating new Employee list with " + amountOfEmployees + " members");
        if (amountOfEmployees > amountOfSymbolsInPassword)
            throw new IllegalArgumentException("too many employees, it is so expensive for us");
        System.out.println("target md5 is " + targetMD5);
        System.out.println("Password max length " + passwordMaxLength);

        List<Employee> employees = new ArrayList<>();
        System.out.println("Amount of Employee = " + amountOfEmployees);
        int[] amountOfStartingChars = createArrayOfStartingCharacter(amountOfEmployees);
        int currentCharacter = min + 1;

        for (int a : amountOfStartingChars) {
            int[] charsForEmployyes = new int[a];
            for (int i = 0; i < a; i++) {
                charsForEmployyes[i] = currentCharacter;
                System.out.println("Character " + ((char) currentCharacter) + " is highlighted");
                currentCharacter++;
            }
            employees.add(new Employee(targetMD5, charsForEmployyes, passwordMaxLength));
        }
        return employees;
    }

    private static int[] createArrayOfStartingCharacter(int amountOfEmployees) {
        int[] amountOfStartingChars = new int[amountOfEmployees];
        Arrays.fill(amountOfStartingChars, amountOfSymbolsInPassword / amountOfEmployees);
        if ((amountOfSymbolsInPassword % amountOfEmployees) > 0) {
            for (int i = 0; i < (amountOfSymbolsInPassword % amountOfEmployees); i++) {
                amountOfStartingChars[i] = amountOfStartingChars[i] + 1;
            }
        }
        return amountOfStartingChars;
    }

    @Override
    public void run() {
        long passwordTried = 0;
        String password = "";
        int[] predict = new int[passwordMaxLength - 1];
//        System.out.println("Predict length = " + predict.length);
        Arrays.fill(predict, 0, predict.length, min);
//        System.out.println("predict filled " + getPredict(predict));
        while (!foundPass) {
            String pass = generatePassword(predict);
            for (int character :
                    startingChar) {
                password = ((char) character) + pass;
                passwordTried++;
//                System.out.println("try pass -" + password + "-");
                String hash = hash(password).toLowerCase();
                if (hash.equals(targetMD5)) {
                    doIfFound(passwordTried, password);
                    foundPass = true;
                }
            }
            if (passwordTried % 10_000_000 == 0)
                System.out.println(Thread.currentThread().getName() + " password tried " + passwordTried / 1_000_000 +
                        "m, current pass:" + password + ".");
            if (updatepredict(predict)) {
                System.out.println(Thread.currentThread().getName() + " passwords end");
                break;
            }
            if (interrupt) break;
        }
        System.out.println(Thread.currentThread().getName() + " is finished" + " password tried "
                + passwordTried + " last pass was: " + password);
    }

    private String generatePassword(int[] predict) {
        String s = "";
        for (int character :
                predict) {
            if (character == min) character = 32;
            s = s + ((char) character);
        }
        return s.trim();
    }

    private boolean updatepredict(int[] predict) {
        predict[0] = predict[0] + 1;
        if (predict[predict.length - 1] > max) {
            return true;
        }
        for (int i = 0; i < predict.length - 1; i++) {
            if (predict[i] == max + 1) {
                predict[i] = min + 1;
                predict[i + 1] = predict[i + 1] + 1;
            } else return false;
        }
        return false;
    }


    private void doIfFound(long passwordTried, String password) {
        System.out.println(" YES I FOUND IT -" + password);
        System.out.println(" Password tried " + passwordTried);
    }

    public String hash(String toHash) {
        Hasher hasher = Hashing.md5().newHasher();
        hasher.putString(toHash, StandardCharsets.UTF_8);
        return hasher.hash().toString();
    }


    String getStartedChats() {
        String s = "";
        for (int character :
                startingChar) {
            s = s + (char) character + " ";
        }
        return s;
    }


    String md5Digest(String s) {
        return new String(msg.digest(s.getBytes()));
    }

    boolean foundPass() {
        return foundPass;
    }

    void interrupt() {
        interrupt = true;
    }
}
