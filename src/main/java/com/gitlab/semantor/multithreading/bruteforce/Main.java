package com.gitlab.semantor.multithreading.bruteforce;

import java.util.List;

class Main {
    private static final String MD5 = "DDC4035FF6451F85BB796879E9E5EA49";

    public static void main(String[] args) {
        List<Employee> employees = Employee.getEmployees(26,
                MD5, 7);
        for (Employee e :
                employees) {
            System.out.println(e.getStartedChats());
        }

        for (Employee employee :
                employees) {
            new Thread(employee).start();
        }

        boolean inProgress = true;
        
        while(inProgress){
            for (Employee e :
                    employees) {
                if (e.foundPass()) {
                    inProgress = false;
                }
            }
        }
        for (Employee e :
                employees) {
            e.interrupt();
        }

    }
}
