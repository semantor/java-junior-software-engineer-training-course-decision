package com.gitlab.semantor.multithreading.philosophers;

import java.util.ArrayList;
import java.util.List;

/**
 * Philosopher with waiter
 */
class Main {
    public static void main(String[] args) {
        List<Fork> forks = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            forks.add(new Fork(i));
        }
        List<Philsopher> philsophers = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            int b = 0;
            if (i != 4) b = i;
            philsophers.add(new Philsopher(forks.get(i), forks.get(
                    (i != 4 ? (i + 1) : 0)), "Philosopher " + i));
        }
        for (Philsopher p :
                philsophers) {
            System.out.println(p.getRightFork().getNumber() + " " + p.getLeftFork().getNumber());
        }
        List<Thread> threads = new ArrayList<>();
        for (Philsopher p :
                philsophers) {
            threads.add(new Thread(p));
        }

        for (Thread t :
                threads) {
            t.start();
        }

        Waiter waiter = new Waiter(philsophers);
        new Thread(waiter).start();
    }
}
