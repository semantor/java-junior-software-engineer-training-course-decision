package com.gitlab.semantor.multithreading.philosophers;

import java.util.List;

class Waiter implements Runnable{

    private final Philsopher[] philsopherWithHat;
    private final int philosopherCount;
    private final List<Philsopher> philsophers;

    Waiter(List<Philsopher> philsophers) {
        this.philsophers=philsophers;
        philosopherCount=philsophers.size();
        this.philsopherWithHat = new Philsopher[philosopherCount/2];
    }

    @Override
    public void run() {
        int i = 1;
        while(true){
            giveHatToPhilosophers(i);
            try {
                Thread.sleep(10_000);
            } catch (InterruptedException ex){
                System.out.println("someone interrupt waiter");
            }
            pickUpHat();
            i++;
        }
    }

    private void pickUpHat() {
        for (Philsopher phil :
                philsopherWithHat) {
            phil.takeHat();
            phil = null;
        }
    }

    private void giveHatToPhilosophers(int i) {
        for (Philsopher phil :
                philsophers) {
            if (phil==null) throw new IllegalStateException(); // CHANGE TO OPTIONAL
        }
        for (int j = 0; j < philsopherWithHat.length; j++) {
            philsopherWithHat[j]=philsophers.get((i+2*j)%philosopherCount);
        }
        for (Philsopher phil :
                philsopherWithHat) {
            phil.giveHat();
        }
    }
}
