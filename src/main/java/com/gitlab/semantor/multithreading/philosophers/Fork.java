package com.gitlab.semantor.multithreading.philosophers;

class Fork {
    private final int number;

    Fork(int number) {
        this.number = number;
    }

    int getNumber() {
        return number;
    }
}
