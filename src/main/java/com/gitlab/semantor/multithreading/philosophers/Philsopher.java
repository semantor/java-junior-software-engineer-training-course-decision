package com.gitlab.semantor.multithreading.philosophers;

class Philsopher implements Runnable {

    private final String name;

    private volatile State state;
    private volatile boolean haveHat;
    private final Fork rightFork;
    private final Fork leftFork;
    private long lastEatingTIme;

    Philsopher(Fork rightFork, Fork leftFork, String name) {
        this.name = name;
        state = State.TALKING;
        this.rightFork = rightFork;
        this.leftFork = leftFork;
        haveHat = false;
    }

    @Override
    public void run() {
        lastEatingTIme = System.currentTimeMillis();
        state = State.TALKING;
        System.out.println("this " + name + "  " + state.toString());
        while (true) {
            if (haveHat) {
                synchronized (rightFork) {
                    System.out.println(name + " take right fork");
                    synchronized (leftFork) {
                        System.out.println(name + " take left fork");
                        state = State.EATING;
                        System.out.println("this " + name + "  " + state.toString());
                        System.out.println(name + " last eating " + (System.currentTimeMillis()-lastEatingTIme)/1000+" sec ago");
                        lastEatingTIme = System.currentTimeMillis();
                        while (haveHat) {
                            try {
                                Thread.sleep(2000);
                            } catch (InterruptedException ex) {
                                System.out.println("Thread was interrupt");
                            }
                        }
                        state = State.TALKING;
                    }
                }
            }
            System.out.println("this " + name + "  " + state.toString());
            while (!haveHat) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {
                    System.out.println("Thread was interrupt");
                }
            }
        }
    }

    Fork getLeftFork() {
        return leftFork;
    }

    Fork getRightFork() {
        return rightFork;
    }

    void takeHat() {
        haveHat = false;
    }

    void giveHat() {
        haveHat = true;
    }

    private enum State {
        EATING("eating"),
        TALKING("talking"),
        WAITING("waiting");

        private final String condition;

        State(String condition) {
            this.condition = condition;
        }

        @Override
        public String toString() {
            return condition;
        }
    }
}
