package com.gitlab.semantor.multithreading.bank.synch;

class Bank {
    private volatile Integer moneyAmount;

    Bank(int startingMoney) {
        moneyAmount = startingMoney;
        System.out.println("new Bank opened with capital" + startingMoney);
    }

     int transferMoney(int amount) {
        if (moneyAmount < amount) {
            System.out.println("No such Money");
            throw new IllegalArgumentException("no such money");
        }
        moneyAmount = moneyAmount - amount;
        return moneyAmount;
    }

     boolean hasMoney(int amount) {
        return (moneyAmount - amount) > 0;
    }
}
