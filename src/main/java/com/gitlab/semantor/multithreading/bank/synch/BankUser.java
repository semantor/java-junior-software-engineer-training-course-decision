package com.gitlab.semantor.multithreading.bank.synch;

class BankUser implements Runnable {

    private final int requiredMoney;
    private final Bank bank;

    BankUser(int requiredMoney, Bank bank) {
        this.requiredMoney = requiredMoney;
        this.bank = bank;
        System.out.println("new BankUser activate with name" + Thread.currentThread().getName());
    }


    @Override
    public void run() {
        while (bank.hasMoney(requiredMoney)) {
            synchronized (bank) {
                if (bank.hasMoney(requiredMoney)) {
                    int b = bank.transferMoney(requiredMoney);
                    System.out.println(requiredMoney + "$ removed, " + b + "$ left");
                }
            }
        }
    }
}
