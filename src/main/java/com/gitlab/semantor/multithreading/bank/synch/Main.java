package com.gitlab.semantor.multithreading.bank.synch;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Main {
    public static void main(String[] args) {
        Bank bank = new Bank(10_000_000);
        List<BankUser> bankUsers = Stream.generate(() -> new BankUser(13, bank)).limit(8)
                .collect(Collectors.toList());
        List<Thread> threads = new ArrayList<>();
        bankUsers.forEach(bankUser -> threads.add(new Thread(bankUser)));
        threads.forEach(Thread::start);
    }
}
