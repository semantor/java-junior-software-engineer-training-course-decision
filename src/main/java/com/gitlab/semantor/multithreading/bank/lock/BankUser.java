package com.gitlab.semantor.multithreading.bank.lock;

import java.util.concurrent.locks.ReentrantLock;

class BankUser implements Runnable {

    private final int requiredMoney;
    private final Bank bank;
    private final ReentrantLock lock;

    BankUser(int requiredMoney, Bank bank, ReentrantLock lock) {
        this.lock = lock;
        this.requiredMoney = requiredMoney;
        this.bank = bank;
        System.out.println("new BankUser activate with name" + Thread.currentThread().getName());
    }


    @Override
    public void run() {
        while (bank.hasMoney(requiredMoney)) {
            lock.lock();
            if (bank.hasMoney(requiredMoney)) {
                int b = bank.transferMoney(requiredMoney);
                System.out.println(requiredMoney + "$ removed, " + b + "$ left");
            }
            lock.unlock();
        }
    }
}
