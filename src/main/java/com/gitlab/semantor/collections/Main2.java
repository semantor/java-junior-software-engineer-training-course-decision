package com.gitlab.semantor.collections;

import com.gitlab.semantor.collections.bench.CollectionBench;

import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Main2 {

    public static void main(String[] args) {
        List<Order> orderList = new ArrayList<>(100);

        // new order with numeric field and filter by value
        orderList = Stream.generate(() -> new Order(Main.OrderStatus.COMPLETED, (int) (Math.random() * 100)))
                .filter(x -> x.getNumericField() > 50).limit(100).collect(toList());

        //Remove duplicate
        Set<Order> orderWithoutDuplicate = new HashSet<>(orderList);

        //QueueHandler
        QueueHandler<Object> queueHandler = new QueueHandler<>();

        //MicroBench. for example Arraylist vs Vector
        CollectionBench.benechmark(new ArrayList<>(), new Vector<>());

    }

    public static class Order extends Main.Order {

        private Integer numericField;

        public Order(Main.OrderStatus status) {
            super(status);
        }

        public Order(Main.OrderStatus status, int number) {
            super(status);
            numericField = number;
        }

        public Integer getNumericField() {
            return numericField;
        }

        @Override
        public String toString() {
            return "Order{" +
                    "numericField=" + numericField +
                    ", status=" + status +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Order order = (Order) o;
            return numericField.equals(order.numericField);
        }

        @Override
        public int hashCode() {
            return Objects.hash(numericField);
        }
    }


}
