package com.gitlab.semantor.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

class QueueHandler<T> {
    private final Queue<T> queue = new LinkedBlockingQueue<>();
    private final ReentrantLock lock = new ReentrantLock(true);

    public QueueHandler() {
    }


    public void addToQueue(T object) {
        lock.lock();
        queue.add(object);
        lock.unlock();
    }

    public void start() {
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Thread thread = new CommonThread(lock, queue);
            thread.setDaemon(true);
            threads.add(thread);
        }
        threads.forEach(Thread::start);
    }

    private static class CommonThread extends Thread {
        private final ReentrantLock lock;
        private final Queue queue;

        private CommonThread(ReentrantLock lock, Queue queue) {
            super();
            this.lock = lock;
            this.queue = queue;
        }

        @Override
        public void run() {
            Object obj = null;
            lock.lock();
            while (true) {
                if (!queue.isEmpty()) {
                    obj = queue.poll();
                }
                lock.unlock();

                // do Something with object
                if (obj!=null) System.out.println(obj.toString());

                try {
                    wait(300);
                } catch (InterruptedException e) {
                    System.out.println(
                            "Incredible error"
                    );
                }
            }
        }
    }

}
