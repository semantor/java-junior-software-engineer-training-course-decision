package com.gitlab.semantor.collections;


import java.util.*;

public class Cache<K, V> {
    private final Map<K, V> cache = new HashMap<>();
    private final DAO dao;

    public Cache() {
        dao = new CarDAO();
    }

    public V get(K k) {
        if (cache.get(k) != null) return cache.get(k);
        if (dao.get(k).isPresent()) {
            cache.put(k, (V) dao.get(k).get());
            return cache.get(k);
        } else throw new IllegalArgumentException("no element");
    }

    public V remove(K k) {
        return cache.remove(k);
    }

    public void update(K k, V v) {
        if (dao.update(v))
            cache.put(k, v);
    }

    public boolean cleanup() {
        cache.clear();
        return true;
    }

    private interface DAO<K, T> {
        Optional<T> get(K k);

        List<T> getAll();

        void add(T t);

        boolean update(T t);

        void delete(T t);
    }

    private class CarDAO<K, C> implements DAO<K, C> {
        @Override
        public Optional<C> get(K k) {
            //Some method get data from db
            return Optional.empty();
        }

        @Override
        public List<C> getAll() {
            //Method to get all data
            return Collections.EMPTY_LIST;
        }

        @Override
        public void add(final C c) {
            //add new data to db
        }

        @Override
        public boolean update(final C c) {
            //update data via id in object c
            return true;
        }

        @Override
        public void delete(final C c) {
            //delete data from db
        }
    }
}
