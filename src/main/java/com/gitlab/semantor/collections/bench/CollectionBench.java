package com.gitlab.semantor.collections.bench;

import java.util.*;
import java.util.function.Consumer;

/**
 * Change instanceof to Visitor Pattern
 */
public class CollectionBench {

    private static final Map<Class,BenchManager> fork= new HashMap<>();

    static{
        fork.put(List.class,ListBench.getBench());
        fork.put(Set.class,SetBench.getBench());
        fork.put(Queue.class,QueueBench.getBench());
        fork.put(Deque.class,DequeBench.getBench());
    }

    public static void benechmark(Collection<Integer> collection1, Collection<Integer> collection2) {
        BenchManager bm1 = getNeededBench(collection1);
        BenchManager bm2 = getNeededBench(collection2);

        //Simple stuped bench

        long vectorTime = 0;
        long arrayTime = 0;

        //Round 1. Adding 100_000_000 element.
        System.out.println("Round 1. Adding 100_000_000 element.");
        System.out.println(result(
                timeBench(collection1, bm1::addElementToCollection),
                timeBench(collection2, bm2::addElementToCollection)));

        //Round 2. Removing 50 objects from collection at the beginning.
        System.out.println("Round 2. Removing 50 objects from collection at the beginning.");
        System.out.println(result(
                timeBench(collection1, bm1::removeFromBegining),
                timeBench(collection2, bm2::removeFromBegining)));


        //Round 3. Perform foreach.
        System.out.println("Round 3. Perform foreach.");
        System.out.println(result(
                timeBench(collection1, bm1::performForEach),
                timeBench(collection2, bm2::performForEach)));
        //Round 4. Perform Stream.
        System.out.println("Round 4. Perform Stream.");
        System.out.println(result(
                timeBench(collection1, bm1::performStream),
                timeBench(collection2, bm2::performStream)));
        //Round 5. Perform Iterator.
        System.out.println("Round 5. Perform Iterator.");
        System.out.println(result(
                timeBench(collection1, bm1::performIterator),
                timeBench(collection2, bm2::performIterator)));

        //Round 6. Perform Search.
        System.out.println("Round 6. Perform Search.");
        System.out.println(result(
                timeBench(collection1, bm1::performSearch),
                timeBench(collection2, bm2::performSearch)));


    }

    private static BenchManager getNeededBench(Collection collection) {
        BenchManager benchManager = fork.get(collection.getClass());
        if (benchManager == null) {
            throw new IllegalArgumentException("unknown Collection" + collection);
        }
        return benchManager;
    }

    private static long timeBench(Collection<Integer> collection, Consumer<Collection> consumer) {
        long startTime = System.currentTimeMillis();
        consumer.accept(collection);
        return System.currentTimeMillis() - startTime;
    }

    private static String result(long firstCollectionTime, long secondCollectionTime) {
        return firstCollectionTime < secondCollectionTime ?
                "First Collection win with time " + firstCollectionTime +
                        " faster for " + (secondCollectionTime - firstCollectionTime) + " millis" :
                "Second Collection win with time " + secondCollectionTime +
                        " faster for " + (firstCollectionTime - secondCollectionTime) + " millis";
    }

}
