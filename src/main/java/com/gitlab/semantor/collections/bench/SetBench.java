package com.gitlab.semantor.collections.bench;

import java.util.Collection;

class SetBench implements BenchManager {

    private SetBench() {
    }

    static BenchManager getBench() {
        return new SetBench();
    }

    @Override
    public void removeFromBegining(Collection<Integer> collection) {
        throw new UnsupportedOperationException("Not supported yet.");

    }

    @Override
    public void removeFromEnd(Collection<Integer> collection) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void addToBegin(Collection<Integer> collection) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void performIndexed(Collection<Integer> collection) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
