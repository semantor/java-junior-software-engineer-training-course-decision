package com.gitlab.semantor.collections.bench;

import java.util.Collection;
import java.util.Iterator;

interface BenchManager {


    default void addElementToCollection(Collection<Integer> collection) {
        for (int i = 0; i < 100_000_000; i++) {
            collection.add(i);
        }
    }

    default void removeFromBegining(Collection<Integer> collection) {
        for (int i = 0; i < 50; i++) {
            collection.remove(i);
        }
    }

    /**
     * Doesnt work in Set yet
     *
     * @param collection
     */
    @Deprecated
    void removeFromEnd(Collection<Integer> collection);

    /**
     * Doesnt work in Set and Queue yet
     *
     * @param collection
     */
    @Deprecated
    void addToBegin(Collection<Integer> collection);

    /**
     * Doesnt work in Set yet
     *
     * @param collection
     */
    void performIndexed(Collection<Integer> collection);

    default void performForEach(Collection<Integer> collection) {
        for (Integer integer :
                collection) {
            integer = integer - 1;
        }
    }

    default void performIterator(Collection<Integer> collection) {
        for (Iterator iterator = collection.iterator(); iterator.hasNext(); ) {
            iterator.next();
        }
    }

    default void performStream(Collection<Integer> collection) {
        collection.stream().reduce(0, (x, y) -> x + y);
    }

    default void performSearch(Collection<Integer> collection) {
        for (int i = 0; i < 200; i++) {
            collection.contains(i * 100_000);
        }
    }

}
