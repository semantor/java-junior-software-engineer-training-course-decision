package com.gitlab.semantor.collections.bench;

import java.util.Collection;
import java.util.Deque;

class DequeBench implements BenchManager {

    private DequeBench() {
    }

    static BenchManager getBench() {
        return new DequeBench();
    }

    @Override
    public void addToBegin(Collection collection) {
        for (int i = 0; i < 50; i++) {
            ((Deque) collection).addFirst(i);
        }
    }

    @Override
    public void removeFromBegining(Collection<Integer> collection) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void removeFromEnd(Collection<Integer> collection) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void performIndexed(Collection<Integer> collection) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
