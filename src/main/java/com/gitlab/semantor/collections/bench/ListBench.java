package com.gitlab.semantor.collections.bench;

import java.util.Collection;
import java.util.List;

class ListBench implements BenchManager {

    private ListBench() {
    }

    static BenchManager getBench() {
        return new ListBench();
    }


    @Override
    public void addToBegin(Collection<Integer> collection) {
        for (int i = 49; i > -1; i++) {
            ((List) collection).add(0, i);
        }
    }

    @Override
    public void performIndexed(Collection<Integer> collection) {
        List<Integer> list = (List<Integer>) collection;
        for (int i = 0; i < collection.size(); i++) {
            list.add(i, list.get(i) - 1);
        }
    }

    @Override
    public void removeFromBegining(Collection<Integer> collection) {
        throw new UnsupportedOperationException("Not supported yet.");

    }

    @Override
    public void removeFromEnd(Collection<Integer> collection) {
        throw new UnsupportedOperationException("Not supported yet.");

    }
}
