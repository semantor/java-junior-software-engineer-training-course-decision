package com.gitlab.semantor.io;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
public class ConsumerBucket implements Serializable {
    @Getter
    @Setter
    private Product product;
}
