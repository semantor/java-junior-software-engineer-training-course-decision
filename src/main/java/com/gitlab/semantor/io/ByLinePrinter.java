package com.gitlab.semantor.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

class ByLinePrinter {

    public static void main(String[] args) {
        ByLinePrinter byLinePrinter = new ByLinePrinter();
        byLinePrinter.printLineByPress();
    }


    void printLineByPress(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Give me Path");
        String s = scanner.next();
        Path path = Paths.get(s);
        System.out.println("Trying to read");
        path = Paths.get("C:/AllJava/Exercise/java-junior-software-engineer-training-course-decision" +
                "/src/main/resources/example.json");
        try (BufferedReader br = Files.newBufferedReader(path)) {
            while (br.ready())
                if (scanner.hasNext()) {
                    scanner.next();
                    System.out.println(br.readLine());
                }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}