package com.gitlab.semantor.io;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

class IOUtilClass {

    void printReversedInformationFromFile(final Path FILE_PATH) {
        final StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader br = Files.newBufferedReader(FILE_PATH)) {
            br.lines().forEach((x)->{
                stringBuilder.append(x+"\n");
            });
        } catch (FileNotFoundException ex) {
            System.out.println("Wrong File name");
        } catch (IOException ex) {
            System.out.println("failed or interrupted I/O operations.");
        }

        stringBuilder.reverse();
        System.out.println(stringBuilder);
    }


    void printReversedByLineInformationFromFile(final Path FILE_Path) {
        final Deque<String> strings = new LinkedList<>();
        try (BufferedReader br = Files.newBufferedReader(FILE_Path)) {
            br.lines().forEach(strings::addFirst);
        } catch (FileNotFoundException ex) {
            System.out.println("Wrong File name");
        } catch (IOException ex) {
            System.out.println("failed or interrupted I/O operations.");
        }
        while (!strings.isEmpty()) {
            System.out.println(strings.poll());
        }
    }

    void writeDataToFile(List<Product> products, Path path) {
        try (DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(path.toFile())))) {
            for (Product product :
                    products) {
                dos.writeUTF(product.getName());
                dos.writeDouble(product.getPrice());
                dos.writeInt(product.getQuantity());
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Wrong File name");
        } catch (IOException ex) {
            System.out.println("failed or interrupted I/O operations.");
        }
    }

    List<Product> readDataFromFile(final Path path) {
        List<Product> products = new ArrayList<>();
        try (DataInputStream dos = new DataInputStream(new BufferedInputStream(new FileInputStream(path.toFile())))) {
            while (dos.available() > 0) {
                products.add(new Product(dos.readUTF(), dos.readDouble(), dos.readInt()));
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Wrong File name");
        } catch (IOException ex) {
            System.out.println("failed or interrupted I/O operations.");
        }
        return products;
    }

    void writeProductsToFile(List<ConsumerBucket> buckets, Path path) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(path.toFile())))) {
            oos.writeObject(buckets);
            System.out.println("Writing is complete");
        } catch (FileNotFoundException ex) {
            System.out.println("Wrong File name");
        } catch (IOException ex) {
            System.out.println("failed or interrupted I/O operations.");
        }
    }

    List<ConsumerBucket> readProductsFromFile(Path path) {
        List<ConsumerBucket> buckets = new ArrayList<>();
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(path.toFile())))) {
            buckets = (List<ConsumerBucket>) ois.readObject();
            System.out.println("Writing is complete");
        } catch (FileNotFoundException ex) {
            System.out.println("Wrong File name");
        } catch (IOException ex) {
            System.out.println("failed or interrupted I/O operations.");
        } catch (ClassNotFoundException ex) {
            System.out.println("wrong class");
        }
        return buckets;
    }

}
