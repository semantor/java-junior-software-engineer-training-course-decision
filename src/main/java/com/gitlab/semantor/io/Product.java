package com.gitlab.semantor.io;

import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@EqualsAndHashCode
@ToString
class Product implements Serializable {
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private double price;
    @Getter
    @Setter
    @EqualsAndHashCode.Exclude private int quantity;

}
