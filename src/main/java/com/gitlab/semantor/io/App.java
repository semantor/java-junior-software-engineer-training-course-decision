package com.gitlab.semantor.io;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class App {
    private static final ProductSpecial cookie = new ProductSpecial("Cookie", 20.2, 100);


    public static void main(String[] args) throws URISyntaxException {
        Path path = Paths.get(ClassLoader.getSystemResource("ObjectTestFile.dat").toURI());
        App app = new App();
        app.writeProductToFile(cookie,path);
        ProductSpecial productSpecial = app.readProductWithoutQuantity(path);
        System.out.println(productSpecial.toString());
    }


    void writeProductToFile(ProductSpecial product, Path path) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(path.toFile())))) {
            oos.writeObject(product);
            System.out.println("Writing is complete");
        } catch (FileNotFoundException ex) {
            System.out.println("Wrong File name");
        } catch (IOException ex) {
            System.out.println("failed or interrupted I/O operations.");
        }
    }

    ProductSpecial readProductWithoutQuantity(Path path) {
        ProductSpecial product = new ProductSpecial();
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(path.toFile())))) {
            product = (ProductSpecial) ois.readObject();
            System.out.println("Writing is complete");
        } catch (FileNotFoundException ex) {
            System.out.println("Wrong File name");
        } catch (IOException ex) {
            System.out.println("failed or interrupted I/O operations.");
        } catch (ClassNotFoundException ex) {
            System.out.println("wrong class");
        }
        return product;
    }
}
