package com.gitlab.semantor.io;

import lombok.*;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@EqualsAndHashCode
@ToString
@RequiredArgsConstructor
public class ProductSpecial implements Externalizable {
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private double price;
    @Getter
    @Setter
    @EqualsAndHashCode.Exclude private int quantity;

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        List<Object> objects = new ArrayList<>();
        objects.add(name);
        objects.add(price);
        objects.add(quantity);
        out.writeObject(objects);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        List<Object> objects = (List<Object>) in.readObject();
        name = (String) objects.get(0);
        price = (Double) objects.get(1);
    }
}
