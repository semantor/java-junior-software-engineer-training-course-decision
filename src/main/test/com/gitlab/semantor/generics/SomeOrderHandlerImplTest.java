package com.gitlab.semantor.generics;

import com.gitlab.semantor.collections.Main;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SomeOrderHandlerImplTest {

    @Test
    void processing() {
        Main.Order input = new Main.Order(Main.OrderStatus.NOT_STARTED);
        Main.Order expected = new Main.Order(Main.OrderStatus.PROCESSING);
        SomeOrderHandlerImpl.getInstance().processing(input);
        assertEquals(expected,input);
    }
}