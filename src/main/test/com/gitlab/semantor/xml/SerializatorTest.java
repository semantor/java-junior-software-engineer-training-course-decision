package com.gitlab.semantor.xml;

import com.gitlab.semantor.xml.json.Data;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;

import javax.management.Attribute;
import javax.xml.parsers.DocumentBuilder;
import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

class SerializatorTest {
    final Serializator serializator = new Serializator();


    @Test
    void readJson() throws URISyntaxException {
        Path path = Paths.get(ClassLoader.getSystemResource("example.json").toURI());
        serializator.readJson(path);
    }

    @Test
    void deserializeAndThenSerialize() throws URISyntaxException {
        File from = new File(ClassLoader.getSystemResource("example.json").toURI());
        File to = new File(ClassLoader.getSystemResource("qwer.json").toURI());
        Data data = (Data) serializator.deserializeJSONFromFile(new Data(), from);
        serializator.serializeJSONInFile(data, to);
        Data dataOutput = (Data) serializator.deserializeJSONFromFile(new Data(), to);
        Assertions.assertEquals(data,dataOutput);
    }

    @Test
    void findNeededAttribute(){
        serializator.findNeededAttribute();
    }

    @Test
    void createDOMTest(){
        Document dom = serializator.createDOM();
        Element documentElement = dom.getDocumentElement();
        Assertions.assertEquals(documentElement.getTagName(),"ns6:UtilityData");
    }

}