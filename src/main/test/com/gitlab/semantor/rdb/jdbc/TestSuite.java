package com.gitlab.semantor.rdb.jdbc;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses(SQLcrudTest.class)
public class TestSuite {

}
