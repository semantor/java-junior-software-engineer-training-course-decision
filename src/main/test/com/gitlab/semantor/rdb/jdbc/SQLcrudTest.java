package com.gitlab.semantor.rdb.jdbc;

import com.gitlab.semantor.rdb.jdbc.entities.City;
import com.gitlab.semantor.rdb.jdbc.entities.Group;
import com.gitlab.semantor.rdb.jdbc.entities.Student;
import com.gitlab.semantor.rdb.jdbc.entities.University;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import java.sql.Date;


public class SQLcrudTest {

    private final static SQLcrud crud = new SQLcrud("jdbc:postgresql://127.0.0.1:5432",
            "postgres","postgres","root","false");
    private static Student anfisa = new Student(10, "Grishina", "Anfisa", "Sergeevna",
            Date.valueOf("1990-09-16"), "female");
    private static Student mihail = new Student(1, "Sidorov", "Mihail",
            "Eduardovich", Date.valueOf("1988-11-20"), "male");

    @BeforeClass
    public static void beforeClass() {
        crud.createTable(new City());
        crud.createTable(new University());
        crud.createTable(new Group());
        crud.createTable(new Student());
        crud.create(new City("Samara"));
        crud.create(new City("Moscow"));
        crud.create(new City("Boston"));
        crud.create(new University(1, "SSAU"));
        crud.create(new University(2, "MSU"));
        crud.create(new University(3, "Boston University"));
        crud.create(new Group(1, "232"));
        crud.create(new Group(1, "331"));
        crud.create(new Group(1, "132"));
        crud.create(new Group(2, "physic"));
        crud.create(new Group(2, "biologic"));
        crud.create(new Group(2, "chemistry"));
        crud.create(new Group(3, "School of Theology"));
        crud.create(new Group(3, "School of Medicine"));
        crud.create(new Group(3, "School of Law"));
        crud.create(new Group(3, "College of Arts and Sciences"));
        crud.create(new Group(3, "School of Education"));
        crud.create(mihail);
        crud.create(new Student(1, "Kuznetsov", "Evgenii",
                "Yaroslavovich", Date.valueOf("1989-04-23"), "male"));
        crud.create(new Student(2, "Gonchar", "Alexei",
                "Andreevich", Date.valueOf("1989-01-14"), "male"));
        crud.create(new Student(2, "Lazarev", "Sergei",
                "Mihailovich", Date.valueOf("1990-05-12"), "male"));
        crud.create(new Student(3, "Muhin", "Zaur",
                "Bogdanovich", Date.valueOf("1990-08-18"), "male"));
        crud.create(new Student(3, "Nikulin", "Zurab",
                "Magamedovich", Date.valueOf("1988-07-15"), "male"));
        crud.create(new Student(4, "ZHukov", "Osip", "Vadimovich",
                Date.valueOf("1989-02-05"), "male"));
        crud.create(new Student(4, "Belyakov", "Eric", "Platonovich",
                Date.valueOf("1989-02-05"), "male"));
        crud.create(new Student(5, "Sherbakov", "Oliver", "Borisovich",
                Date.valueOf("1990-05-24"), "male"));
        crud.create(new Student(5, "Tihonov", "Filipp", "Stanislavovich",
                Date.valueOf("1990-11-10"), "male"));
        crud.create(new Student(6, "Komarov", "Vadim", "Usupovich",
                Date.valueOf("1990-04-08"), "male"));
        crud.create(new Student(6, "Osipov", "Taras", "Borisovich",
                Date.valueOf("1989-03-02"), "male"));
        crud.create(new Student(7, "Grishin", "Semen", "Alexandrovich",
                Date.valueOf("1988-09-06"), "male"));
        crud.create(new Student(7, "Bespalov", "Ignat", "Sergeevich",
                Date.valueOf("1989-10-25"), "male"));
        crud.create(new Student(8, "Markov", "Artur", "Valerevich",
                Date.valueOf("1990-01-14"), "male"));
        crud.create(new Student(9, "Orehov", "Yan", "Bogdanovich",
                Date.valueOf("1990-11-17"), "male"));
        crud.create(anfisa);
        crud.create(new Student(10, "Logunova", "Svetlana", "Viktorovna",
                Date.valueOf("1991-11-21"), "female"));
        crud.createFunction();
        crud.createTrigger();
    }


    @AfterClass
    public static void afterClass() {
        crud.dropTable(Student.tableName);
        crud.dropTable(Group.tableName);
        crud.dropTable(University.tableName);
        crud.dropTable(City.tableName);
        crud.dropFunction();
        crud.dropTrigger();
    }


    @Test
    public void read() {
        Student input = new Student(17);
        System.out.println(crud.read(input).toString());
        assertEquals(anfisa, crud.read(input));
    }

    @Test
    public void update() {
        City sidney = new City(3, "Sidney");
        crud.update(sidney);
        System.out.println(crud.read(sidney).toString());
        assertEquals(sidney, crud.read(sidney));
    }

    @Test
    public void remove() {
        Student input = new Student(1);
        System.out.println("Delete = "+crud.read(input));
        crud.remove(input);
        System.out.println("after removal = "+crud.read(input));
        Student expected = new Student(1);
        assertEquals(
                expected,
                crud.read(new Student(1)));
    }

    @Test
    public void testFunction() {
        String s = "4 Sergei Lazarev\n" +
                "5 Zaur Muhin\n" +
                "9 Oliver Sherbakov\n" +
                "10 Filipp Tihonov\n" +
                "11 Vadim Komarov\n" +
                "15 Artur Markov\n" +
                "16 Yan Orehov\n" +
                "17 Anfisa Grishina\n" +
                "18 Svetlana Logunova\n";
        assertEquals(s,crud.useFunction());
    }

    @Test
    public void testTrigger() {
        crud.remove(new Student(17));
    }

    @Test
    public void getMinAgeTest() {
        System.out.println(crud.getMinAge());
        assertEquals(Date.valueOf("1991-11-21"),crud.getMinAge());
    }

    @Test
    public void getMaxAgeTest() {
        System.out.println(crud.getMaxAge());
        assertEquals(Date.valueOf("1988-07-15"),crud.getMaxAge());
    }

    @Test
    public void testJoin() {
        System.out.println(crud.createStudentsGroupTable());
        assertEquals("College of Arts and Sciences Anfisa Grishina",
                crud.createStudentsGroupTable());
    }



}