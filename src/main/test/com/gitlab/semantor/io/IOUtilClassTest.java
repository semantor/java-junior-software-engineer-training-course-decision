package com.gitlab.semantor.io;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

class IOUtilClassTest {
    private static final IOUtilClass util = new IOUtilClass();
    private static final Product cookie = new Product("Cookie", 20.2, 100);

    @Test
    void printReversedInformationFromFile() throws URISyntaxException {
        Path path = Paths.get(ClassLoader.getSystemResource("example.json").toURI());
        util.printReversedInformationFromFile(path);
    }

    @Test
    void printReversedByLineInformationFromFile() throws URISyntaxException {
        Path path = Paths.get(ClassLoader.getSystemResource("example.json").toURI());
        util.printReversedByLineInformationFromFile(path);
    }

    @Test
    void writeAndReadDataFromFile() throws URISyntaxException {
        Path path = Paths.get(ClassLoader.getSystemResource("DataTestFile.dat").toURI());
        List<Product> products = new ArrayList<>();
        products.add(cookie);
        products.add(new Product("Fairy", 150.0, 10));
        util.writeDataToFile(products, path);
        List<Product> products2 = util.readDataFromFile(path);
        products.forEach((x) -> System.out.println(x.toString()));
        Assertions.assertEquals(products,products2);
    }

    @Test
    void readProductsFromFile() throws URISyntaxException {
        Path path = Paths.get(ClassLoader.getSystemResource("ObjectTestFile.dat").toURI());
        List<ConsumerBucket> buckets = new ArrayList<>();
        buckets.add(new ConsumerBucket(cookie));
        buckets.add(new ConsumerBucket(cookie));
        util.writeProductsToFile(buckets, path);
        buckets = util.readProductsFromFile(path);
        Assertions.assertSame(buckets.get(0).getProduct(), buckets.get(1).getProduct());
    }



}