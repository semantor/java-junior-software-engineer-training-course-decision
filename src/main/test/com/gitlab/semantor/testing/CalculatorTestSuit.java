package com.gitlab.semantor.testing;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        CalculatorTestNegative.class,
        CalculatorTestPositive.class
})

public class CalculatorTestSuit {

}
