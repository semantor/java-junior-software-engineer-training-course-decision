package com.gitlab.semantor.testing;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberMatcher;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;


@RunWith(PowerMockRunner.class)
@PrepareForTest({NumberRowSupplier.class})
@PowerMockIgnore({"com.sun.org.apache.xerces.*", "javax.xml.*", "org.xml.*", "javax.management.*","org.w3c.dom.*"})

public class CalculatorTestPositive {

    @Test
    public void addition() {
        assertEquals(4., Calculator.addition(2, 2));
    }

    @Test
    public void multiplication() {
        assertEquals(4., Calculator.multiplication(2, 2));
    }

    @Test
    public void subtraction() {
        assertEquals(2., Calculator.subtraction(4, 2));
    }

    @Test
    public void division() {
        assertEquals(2., Calculator.division(4, 2));
    }

    @Test
    public void sqrt() {
        assertEquals(2., Calculator.sqrt(4));
    }

    @Test(timeout = 500)
    public void isPrime59() {
        assertTrue(Calculator.isPrime(59));
    }

    @Test(timeout = 500)
    public void isPrime3() {
        assertTrue(Calculator.isPrime(3));
    }

    @Test(timeout = 500)
    public void isPrime99() {
        assertFalse(Calculator.isPrime(99));
    }

    @Test(timeout = 500)
    public void isPrime3571() {
        assertTrue(Calculator.isPrime(3571));
    }

    @Test
    public void numberOfFibonacciForZero() {
        assertEquals(0, Calculator.numberOfFibonacci(0));
    }

    @Test
    public void numberOfFibonacciForOne() {
        assertEquals(1, Calculator.numberOfFibonacci(1));
    }

    @Test
    public void numberOfFibonacciForTwo() {
        assertEquals(1, Calculator.numberOfFibonacci(2));
    }

    @Test
    public void numberOfFibonacciForThree() {
        assertEquals(2, Calculator.numberOfFibonacci(3));
    }

    @Test
    public void numberOfFibonacciForFour() {
        assertEquals(3, Calculator.numberOfFibonacci(4));
    }

    @Test
    public void numberOfFibonacciForTen() {
        assertEquals(55, Calculator.numberOfFibonacci(10));
    }

    @Test
    public void numberOfFibonacciForThirty() {
        assertEquals(832040, Calculator.numberOfFibonacci(30));
    }


    @Test
    public void summarizeNumberRowTestWithMockGoodMethod() {
        NumberRowSupplier numberRowSupplier = Mockito.mock(NumberRowSupplier.class);
        int[] array = new int[]{24, 25, 26};
        Mockito.when(numberRowSupplier.goodMethod()).thenReturn(array);
        assertEquals(75, Calculator.summarizeNumberRow(numberRowSupplier.goodMethod()));
    }

    @Test
    public void summarizeNumberRowTestWithSpyGoodMethod() {
        NumberRowSupplier numberRowSupplier = Mockito.spy(NumberRowSupplier.class);
        int[] array = new int[]{24, 25, 26};
        Mockito.when(numberRowSupplier.goodMethod()).thenReturn(array);
        assertEquals(75, Calculator.summarizeNumberRow(numberRowSupplier.goodMethod()));
    }

    @Test
    public void summarizeNumberRowTestWithPowerMockFinalMethod(){
        NumberRowSupplier mock = PowerMockito.mock(NumberRowSupplier.class);
        int[] array = new int[]{24, 25, 26};
        PowerMockito.when(mock.finalMethod()).thenReturn(array);
        assertEquals(75, Calculator.summarizeNumberRow(mock.finalMethod()));
    }

    @Test
    public void summarizeNumberRowTestWithPowerMockStaticMethod(){
        PowerMockito.mockStatic(NumberRowSupplier.class);
        int[] array = new int[]{24, 25, 26};
        PowerMockito.when(NumberRowSupplier.staticMethod()).thenReturn(array);
        assertEquals(75, Calculator.summarizeNumberRow(NumberRowSupplier.staticMethod()));
    }

    @Test
    public void summarizeNumberRowTestWithPowerMockPrivateMethod() throws Exception{
        NumberRowSupplier spy = PowerMockito.spy(new NumberRowSupplier());
        int[] array = new int[]{24, 25, 26};
        PowerMockito.when(spy, MemberMatcher.method(NumberRowSupplier.class,"privateMethod")).withNoArguments().thenReturn(array);
        assertEquals(75, Calculator.summarizeNumberRow(spy.publicForPrivateMethod()));
    }

}