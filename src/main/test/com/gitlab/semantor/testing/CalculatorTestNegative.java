package com.gitlab.semantor.testing;

import org.junit.Test;

public class CalculatorTestNegative {

    @Test(expected = IllegalArgumentException.class)
    public void additionSecondNegativeInf() {
        Calculator.addition(2, Double.NEGATIVE_INFINITY);
    }

    @Test(expected = IllegalArgumentException.class)
    public void additionFirstNegativeInf() {
        Calculator.addition(Double.NEGATIVE_INFINITY, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void additionSecondPositiveInf() {
        Calculator.addition(2, Double.POSITIVE_INFINITY);
    }

    @Test(expected = IllegalArgumentException.class)
    public void additionFirstPositiveInf() {
        Calculator.addition(Double.POSITIVE_INFINITY, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void additionFirstNAN() {
        Calculator.addition(Double.NaN, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void additionSecondNAN() {
        Calculator.addition(2, Double.NaN);
    }


    @Test(expected = IllegalArgumentException.class)
    public void multiplicationSecondNegativeInf() {
        Calculator.multiplication(2, Double.NEGATIVE_INFINITY);
    }

    @Test(expected = IllegalArgumentException.class)
    public void multiplicationFirstNegativeInf() {
        Calculator.multiplication(Double.NEGATIVE_INFINITY, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void multiplicationSecondPositivInf() {
        Calculator.multiplication(2, Double.POSITIVE_INFINITY);
    }

    @Test(expected = IllegalArgumentException.class)
    public void multiplicationFirstPositivInf() {
        Calculator.multiplication(Double.POSITIVE_INFINITY, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void multiplicationFirstNAN() {
        Calculator.multiplication(Double.NaN, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void multiplicationSecondNAN() {
        Calculator.multiplication(2, Double.NaN);
    }


    @Test(expected = IllegalArgumentException.class)
    public void subtractionSecondNegativeInf() {
        Calculator.subtraction(2, Double.NEGATIVE_INFINITY);
    }

    @Test(expected = IllegalArgumentException.class)
    public void subtractionFirstNegativeInf() {
        Calculator.subtraction(Double.NEGATIVE_INFINITY, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void subtractionSecondPositivInf() {
        Calculator.subtraction(2, Double.POSITIVE_INFINITY);
    }

    @Test(expected = IllegalArgumentException.class)
    public void subtractionFirstPositivInf() {
        Calculator.subtraction(Double.POSITIVE_INFINITY, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void subtractionFirstNAN() {
        Calculator.subtraction(Double.NaN, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void subtractionSecondNAN() {
        Calculator.subtraction(2, Double.NaN);
    }



    @Test(expected = IllegalArgumentException.class)
    public void divisionSecondNegativeInf() {
        Calculator.division(2, Double.NEGATIVE_INFINITY);
    }

    @Test(expected = IllegalArgumentException.class)
    public void divisionFirstNegativeInf() {
        Calculator.division(Double.NEGATIVE_INFINITY, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void divisionSecondPositivInf() {
        Calculator.division(2, Double.POSITIVE_INFINITY);
    }

    @Test(expected = IllegalArgumentException.class)
    public void divisionFirstPositivInf() {
        Calculator.division(Double.POSITIVE_INFINITY, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void divisionFirstNAN() {
        Calculator.division(Double.NaN, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void divisionSecondNAN() {
        Calculator.division(2, Double.NaN);
    }

    @Test(expected = IllegalArgumentException.class)
    public void divisionSecondZero() {
        Calculator.division(2, 0);
    }



    @Test(expected = IllegalArgumentException.class)
    public void sqrt() {
        Calculator.sqrt(-1);
    }


    @Test(expected = IllegalArgumentException.class)
    public void isPrime() {
        Calculator.isPrime(-1);
    }


    @Test(expected = IllegalArgumentException.class)
    public void numberOfFibonacci() {
        Calculator.numberOfFibonacci(-1);
    }
}