package com.gitlab.semantor.reflection;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DeprecateObjectFinderTest {

    @Test
    void searchEmptyDeprecateObject() {
        DeprecateObjectFinder deprecateObjectFinder = new DeprecateObjectFinder();
        List<Object> input = new ArrayList<>();
        List<Object> expected = new ArrayList<>();
        assertEquals(expected, deprecateObjectFinder.searchDeprecateObject(input));
    }

    @Test
    void searchSimpleDeprecatedObject() {
        DeprecateObjectFinder deprecateObjectFinder = new DeprecateObjectFinder();
        List<Object> input = new ArrayList<>();
        Observable obs = new Observable();
        input.add(obs);
        List<Object> output = new ArrayList<>();
        output.add(obs);
        assertEquals(output, deprecateObjectFinder.searchDeprecateObject(input));
    }
}