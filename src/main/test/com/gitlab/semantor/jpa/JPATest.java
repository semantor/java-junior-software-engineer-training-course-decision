package com.gitlab.semantor.jpa;

import com.gitlab.semantor.jpa.entity.Flight;
import com.gitlab.semantor.jpa.entity.GeneralTable;
import com.gitlab.semantor.jpa.entity.Passenger;
import com.gitlab.semantor.jpa.entity.Ticket;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class JPATest {

    private static GeneralTable expectedGeneralTable;
    private static List<Passenger> expectedPassengers = new ArrayList<>();

        static {
            List<Object[]> lists = new ArrayList<>();
            Object[] objects = {"Vasiliy", "Pupkin", "Samara", "Moscow"};
            Object[] objects1 = {"Petr", "Vasechkin", "Samara", "Moscow"};
            Object[] objects2 = {"Ivan", "Susami", "Samara", "Moscow"};
            Object[] objects3 = {"Innokentii", "Akakiev", "Samara", "Moscow"};
            Object[] objects4 = {"Akakii", "Innokentiev", "Samara", "Moscow"};
            lists.add(objects);
            lists.add(objects1);
            lists.add(objects2);
            lists.add(objects3);
            lists.add(objects4);
            expectedGeneralTable = GeneralTable.createGT(lists);
        }


    @BeforeClass
    public static void beforeClass() {
        Flight flight1 = new Flight("Samara", "Moscow", new ArrayList<>());
        Flight flight2 = new Flight("Samara", "Taiti", new ArrayList<>());
        Passenger passenger1 = new Passenger("Vasiliy", "Pupkin", Date.valueOf("1967-07-15"));
        Passenger passenger2 = new Passenger("Petr", "Vasechkin", Date.valueOf("1987-01-05"));
        Passenger passenger3 = new Passenger("Ivan", "Susami", Date.valueOf("1992-03-12"));
        Passenger passenger4 = new Passenger("Innokentii", "Akakiev", Date.valueOf("1965-02-22"));
        Passenger passenger5 = new Passenger("Akakii", "Innokentiev", Date.valueOf("1974-11-28"));

        Ticket ticket1 = new Ticket("1A", flight1, passenger1);
        Ticket ticket2 = new Ticket("4D", flight1, passenger2);
        Ticket ticket3 = new Ticket("6C", flight1, passenger3);
        Ticket ticket4 = new Ticket("8E", flight1, passenger4);
        Ticket ticket5 = new Ticket("10B", flight1, passenger5);

        passenger1.setTicket(ticket1);
        passenger2.setTicket(ticket2);
        passenger3.setTicket(ticket3);
        passenger4.setTicket(ticket4);
        passenger5.setTicket(ticket5);

        expectedPassengers.add(passenger4);
        expectedPassengers.add(passenger1);
        expectedPassengers.add(passenger5);
        expectedPassengers.add(passenger2);
        expectedPassengers.add(passenger3);

        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.save(flight1);
            session.save(flight2);
            session.save(ticket1);
            session.save(ticket2);
            session.save(ticket3);
            session.save(ticket4);
            session.save(ticket5);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }


    }


    @Test
    public void createGeneralTableWithHQLTest() {
        List<Object[]> results = new JPQLDAO().createGeneralTable();
        GeneralTable gt = GeneralTable.createGT(results);
        System.out.println(gt.toString());
        Assert.assertEquals(expectedGeneralTable,gt);
    }

    @Test
    public void passengersOrderByBirthdayWithHQLTest() {
        List<Passenger> results = new JPQLDAO().passengersOrderByBirthday();
        results.forEach(System.out::println);
        Assert.assertEquals(expectedPassengers,results);
    }

    @Test
    public void createGeneralTableWithNativeTest() {
        List<Object[]> results = new NativeQueryDAO().createGeneralTable();
        GeneralTable gt = GeneralTable.createGT(results);
        System.out.println(gt.toString());
        Assert.assertEquals(expectedGeneralTable,gt);

    }

    @Test
    public void passengersOrderByBirthdayWithNativeTest() {
        List<Passenger> results = new NativeQueryDAO().passengersOrderByBirthday();
        results.forEach(System.out::println);
        Assert.assertEquals(expectedPassengers,results);
    }

    @Test
    public void createGeneralTableWithNamedTest() {
        List<Object[]> results = new NamedQueryDAO().createGeneralTable();
        GeneralTable gt = GeneralTable.createGT(results);
        System.out.println(gt.toString());
        Assert.assertEquals(expectedGeneralTable,gt);

    }

    @Test
    public void passengersOrderByBirthdayWithNamedTest() {
        List<Passenger> results = new NamedQueryDAO().passengersOrderByBirthday();
        results.forEach(System.out::println);
        Assert.assertEquals(expectedPassengers,results);
    }

    @Test
    public void createGeneralTableWithCriteriaTest() {
        List<Object[]> results = new CriteriaDAO().createGeneralTable();
        GeneralTable gt = GeneralTable.createGT(results);
        System.out.println(gt.toString());
        Assert.assertEquals(expectedGeneralTable,gt);
    }

    @Test
    public void passengersOrderByBirthdayWithCriteriaTest() {
        List<Passenger> results = new CriteriaDAO().passengersOrderByBirthday();
        results.forEach(System.out::println);
        Assert.assertEquals(expectedPassengers,results);
    }


}
